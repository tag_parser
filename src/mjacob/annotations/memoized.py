# This Python file uses the following encoding: utf-8
'''
Created on May 12, 2011

a simple memoization routine

@author: mjacob
'''

def Memoize(func):
    """replace an function without arguments with a function that merely returns the result"""
    def f1(selv):
        value = func(selv)
        def f2():
            return value
        setattr(selv, func.__name__, f2)
        return value
    return f1

if __name__ == "__main__":
    """test functionality"""
    class foobar(object):
        def __init__(self, v):
            self.__v = v
        
        @Memoize
        def thing(self):
            print("computing %s" % (self.__v))
            return 100*self.__v

    a = foobar(1)
    b = foobar(2)
    print("starting")
    print(a.thing)
    print(a.thing())
    print(a.thing)
    print(a.thing())
    print(a.thing)
    print(b.thing())
    print(b.thing())