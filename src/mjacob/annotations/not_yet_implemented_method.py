# This Python file uses the following encoding: utf-8
'''
Created on May 14, 2011

@author: mjacob
'''
class NotYetImplementedException(Exception): pass

def NotYetImplemented(func):
    """used to mark methods which have not yet been implemented"""
    
    def error(*args, **kargs):
        raise NotYetImplementedException("this method is not yet implemented.")
    return error