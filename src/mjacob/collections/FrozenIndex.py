# This Python file uses the following encoding: utf-8
'''
Created on Jun 2, 2011

@author: mjacob
'''
from itertools import chain

class FrozenIndex(object):
    """immutable collection of objects "indexed" by other objects"""
    
    EMPTY = frozenset()
    
    def __init__(self, items):
        d = {}
        for k,v in items:
            if k in d:
                d[k].append(v)
            else:
                d[k] = [v]
        for k in d:
            d[k] = frozenset(d[k])
        self.__dict = d
        self.__values = frozenset(list(self.values()))
        self.__hash = hash(tuple(items))
    
    def __contains__(self, k):
        return k in self.__values
    
    def __getitem__(self, k):
        if k in self.__dict:
            return self.__dict[k]
        else:
            return FrozenIndex.EMPTY
    
    def __iter__(self):
        return iter(self.values())
    
    def __len__(self):
        return len(list(self.values()))
    
    def get(self, k):
        if k in self.__dict:
            return self.__dict.get(k)
        else:
            return FrozenIndex.EMPTY
    
    def has_key(self, k):
        return k in self.__dict
    
    def items(self):
        return tuple(((k,v) for k in self.__dict for v in self.__dict[k]))
    
    def iteritems(self):
        return (((k,v) for k in self.__dict for v in self.__dict[k]))
    
    def iterkeys(self):
        return iter(self.__dict.keys())
    
    def itervalues(self):
        return chain(*list(self.__dict.values()))
    
    def keys(self):
        return list(self.__dict.keys())
    
    def values(self):
        return tuple(chain(*list(self.__dict.values())))
    
    def hash(self):
        return self.__hash
    
    def __eq__(self, othr):
        return self.__dict == othr.__dict
    
    def __ge__(self, othr):
        return self.__dict >= othr.__dict
    
    def __gt__(self, othr):
        return self.__dict > othr.__dict
    
    def __le__(self, othr):
        return self.__dict <= othr.__dict
    
    def __lt__(self, othr):
        return self.__dict < othr.__dict
    
    def __ne__(self, othr):
        return self.__dict != othr.__dict
    
    def __sizeof__(self):
        return self.__dict.__sizeof__()
    
    def __repr__(self):
        return repr(self.__dict)
    
    def __str__(self):
        return str(self.__dict)