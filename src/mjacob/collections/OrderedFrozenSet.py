# This Python file uses the following encoding: utf-8
'''
Created on Jun 2, 2011

@author: mjacob
'''

class OrderedFrozenSet(tuple):
    """immutable collection which keeps items in an order, but which supports
    efficient use of "in" """
    def __init__(self, items):
        unique = []
        seen = set()
        for item in items:
            if not item in seen:
                seen.add(item)
                unique.append(item)

        super(OrderedFrozenSet, self).__init__(unique)
        self.__frozenset = frozenset(items)
    
    def __contains__(self, item):
        return item in self.__frozenset 