# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011

@author: mjacob
'''
from nltk.grammar import Nonterminal
import re

class InvalidTagNonterminalFormatException(Exception): pass

class TagNonterminal(Nonterminal):
    """class for expanding the metadata at a node in a tag _tree.
    allows easy querying and representation of label, foot status, and adjunction requirements"""
    
    NO_ADJUNCTION = 'N'
    OBLIGATORY_ADJUNCTION = 'O'
    IS_FOOT = '*'
    NODE_TAG = re.compile('(\%s)?(\w+)(?:\.([%s%s]))?$' % (IS_FOOT, NO_ADJUNCTION, OBLIGATORY_ADJUNCTION)) 
        
    def __init__(self, string):
        if not isinstance(string, str):
            raise InvalidTagNonterminalFormatException("unexpected type: %s %s" % (type(string), string))
        m = re.match(TagNonterminal.NODE_TAG, string)
        if not m:
            raise InvalidTagNonterminalFormatException("unrecognized tree node format: '%s'" % (string))
        self.__foot = m.group(1) == TagNonterminal.IS_FOOT
        Nonterminal.__init__(self, m.group(2))
        self.__rule = m.group(3) # N if NO ADJUNCTION, O of OBLIGATORY ADJUNCTION
    
    def is_foot(self):
        return self.__foot
    
    def no_adjunction(self):
        """true if adjunction is never allowed at the node"""
        return self.__rule == TagNonterminal.NO_ADJUNCTION
    
    def obligatory_adjunction(self):
        """true if adjunction is MANDATORY at the node"""
        return self.__rule == TagNonterminal.OBLIGATORY_ADJUNCTION
    
    def __str__(self):
        if self.__foot:
            f = TagNonterminal.IS_FOOT
        else:
            f = ""
        if self.no_adjunction():
            r = "." + TagNonterminal.NO_ADJUNCTION
        elif self.obligatory_adjunction():
            r = "." + TagNonterminal.OBLIGATORY_ADJUNCTION
        else:
            r = ""
        return "%s%s%s" % (f, self.symbol(), r)
    
    def __repr__(self):
        return str(self)
    
    def __eq__(self, other):
        return (type(self) is type(other)
            and Nonterminal.__eq__(self, other)
            and self.__foot == other.__foot 
            and self.__rule == other.__rule)
    
    def __hash__(self):
        return hash(self.__foot) ^ hash(self.__rule) ^ Nonterminal.__hash__(self)