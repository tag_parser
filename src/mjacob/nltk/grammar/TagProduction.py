# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011

Production rule for a tree-adjoining grammar.
Based on nltk.grammar.Production

TAG productions are more complicated than context free productions. 
While context-free productions describe rules for rewriting strings,
TAG productions can be more intuitively understood as rules for rewriting
a parse tree. 

Each TAG production is a full tree representing part of the derivation
of a parse of a tree adjoining language. The nodes in the tree are analogous
to non-terminal elements in a context free production, but are a bit more
complicated. The leaves of the tree can be either lexical strings or non-terminal
elements.  

There are two basic types of TAG productions, commonly called "initial" and 
"auxiliary". I will introduce 4 trees, α through δ, to discuss these.

 α:  S    β:  NP    γ:  VP    δ:  VP
    / \       |         |        / \
   NP VP    "John"    "ate"   *VP  "quickly"
   
Trees α, β and γ are initial trees. Tree δ is an auxiliary tree. 
Initial trees may be attached to each other by "substituting" a 
non-terminal leaf node with an initial tree bearing that same non-terminal
as its root. Auxiliary trees may be attached to other trees by the process
of "adjunction", whereby an intermediate node (say, VP) is replaced w/ the 
auxiliary tree, and any children of that node replace the "foot" of the 
auxiliary tree, which is a non-terminal leaf marked in this example as "*VP".

@author: mjacob
'''
from nltk.tree import Tree, ImmutableTree
from nltk.grammar import Production, Nonterminal
import re
from mjacob.annotations.not_yet_implemented_method import NotYetImplemented
from mjacob.collections.FrozenIndex import FrozenIndex
from mjacob.collections.OrderedFrozenSet import OrderedFrozenSet
from mjacob.annotations.memoized import Memoize
from mjacob.nltk.grammar.TagNonterminal import TagNonterminal

IS_LEAF = re.compile("(?:'(.*)')$")

class TagProduction(ImmutableTree):
    """A """
    def __new__(cls, *args):
        return list.__new__(cls)
    
    def __init__(self, rule):
        """
        arguments:
          rule: may be a CFG Production (nltk.grammar.Production), 
                or a Tree (nltk.tree.Tree), or a string representing
                an nltk Tree.
                
                TagNonterminals can have the additional options indicators:
                *X: this is a foot node
                X.N: No adjunction is allowed
                X.O: Adjunction is mandatory at this node
        """
        if isinstance(rule, Production):
            tree = TagProduction._convert_cfg_rule(rule)
            
        elif isinstance(rule, Tree):
            tree = TagProduction._convert_tree(Tree.convert(rule)) # make a copy
            
        else:
            tree = TagProduction._convert_tree(Tree(rule))

        super(TagProduction, self).__init__(tree.node, tree)
        
    def root(self):
        """the root of the production (the root of the tree)"""
        return self.node
    
    @Memoize
    def is_auxiliary(self):
        """True iff the production is an auxiliary tree"""
        return self.foot_treeposition() is not None
    
    @Memoize
    def is_initial(self):
        """True iff the production is an initial (non-auxiliary) tree"""
        return self.foot_treeposition() is None
    
    @Memoize
    def is_lexical(self):
        """True iff the production contains a Terminal (e.g. a lexical item)"""
        if self.terminals():
            return True
        return False
    
    @Memoize
    def is_nonlexical(self):
        """True iff the production contains no lexical items"""
        return not self.is_lexical()

    @NotYetImplemented
    def is_chomsky_normal_form(self): pass
    
    @NotYetImplemented
    def is_binarised(self): pass
    
    @NotYetImplemented
    def is_flexible_chomsky_normal_form(self): pass    

    @NotYetImplemented
    def is_nonempty(self): pass
    
    ############ stuff in the production
    @Memoize
    def treepositions(self):
        """an OrderedFrozenSet of indices of nodes/leaves in this production"""
        return OrderedFrozenSet(super(TagProduction, self).treepositions())
    
    @Memoize
    def pos_subtrees(self):
        """an OrderedFrozenSet of index/subtree tuples"""
        return OrderedFrozenSet((pos, self[pos])
                                for pos in self.treepositions())

    ############ stuff at leaf positions
    @Memoize
    def leaf_treepositions(self):
        """an OrderedFrozenSet of the position of leaves of the production"""
        return OrderedFrozenSet(pos 
                                for pos, subtree in self.pos_subtrees() 
                                if not isinstance(subtree, Tree))
    
    @Memoize
    def leaves(self):
        """an OrderedFrozenSet of the leaves of the production"""
        return OrderedFrozenSet(super(TagProduction, self).leaves())

    @Memoize
    def foot_treeposition(self):
        """If the production is auxiliary, the index of the foot. Otherwise, None"""
        for pos in self.leaf_treepositions():
            subtree = self[pos]
            if isinstance(subtree, TagNonterminal) and subtree.is_foot():
                return pos
        
        return None

    @Memoize
    def terminal_treepositions(self):
        """an OrderedFrozenSet of terminal (lexical) positions"""
        term_positions = []
        
        for pos in self.leaf_treepositions():
            node = self[pos]
            if not isinstance(node, TagNonterminal):
                term_positions.append(pos)
        
        return OrderedFrozenSet(term_positions)
    
    @Memoize
    def terminals(self):
        """an OrderedFrozenSet of the terminals (lexical elements)"""
        return OrderedFrozenSet(self[pos] for pos in self.terminal_treepositions())
        
    @Memoize
    def substitution_treepositions(self):
        """an OrderedFrozenSet substitution positions"""
        sub_positions = []
        
        for pos in self.leaf_treepositions():
            node = self[pos]
            if isinstance(node, TagNonterminal) and not node.is_foot():
                symbol = node.symbol()
                sub_positions.append((symbol, pos))
        
        return FrozenIndex(sub_positions)
    
    ############ stuff not at leaf positions
    @Memoize 
    def internal_treepositions(self):
        """an OrderedFrozenSet of non-leaf treepositions"""
        return OrderedFrozenSet(pos 
                                for pos, subtree in self.pos_subtrees() 
                                if isinstance(subtree, Tree))
        
    @Memoize
    def epsilon_treepositions(self):
        """an OrderedFrozenSet of the positions of nodes without children"""
        epsilon_positions = []
        
        for pos, subtree in self.pos_subtrees():
            if isinstance(subtree, Tree) and len(subtree) == 0:
                epsilon_positions.append(pos)
        
        return OrderedFrozenSet(epsilon_positions)
    
    @Memoize
    def adjunction_treepositions(self):
        """an OrderedFrozenSet of treepositions where adjunction is admissible"""
        adj_positions = []
        
        for pos in self.treepositions():
            node = self.get_node(pos)
            if isinstance(node, TagNonterminal) and not node.is_foot() and not node.no_adjunction():
                symbol = node.symbol()
                adj_positions.append((symbol, pos))
        
        return FrozenIndex(adj_positions)
    
    ############# other stuff
    def get_node(self, p):
        """returns the node at position p"""
        subtree = self[p]
        
        if isinstance(subtree, Tree):
            return subtree.node
        else:
            return subtree
    
    def can_adjoin(self, gamma, p):
        """returns true if adjunction of beta (this tree) is allowed at gamma[p].
        
           note to self:
               so... should fSA be defined in the grammar spec somewhere?
               currently just assume the most general case i.e. labels match and adjunction
               not explicitly forbidden.           """
        node = gamma[p]
        if isinstance(node, Tree):
            node = node.node
        
        return (self.is_auxiliary()
            and type(node) == TagNonterminal 
            and not node.no_adjunction()
            and self.root().symbol() == node.symbol())
        
    ############## construction helpers
    @classmethod
    def _convert_tree(cls, tree):
        """helper method to substitute TagNonterminals for all the node labels"""
        
        for subtree in tree.subtrees():
            if isinstance(subtree.node, TagNonterminal):
                subtree.node = subtree.node
            if isinstance(subtree.node, Nonterminal):
                subtree.node = TagNonterminal(subtree.node.symbol())
            else:
                subtree.node = TagNonterminal(subtree.node)
        
        for leaf, position in ((tree[tree.leaf_treeposition(i)], tree.leaf_treeposition(i)) 
                               for i in range(len(tree.leaves()))):
            
            m = re.match(IS_LEAF, leaf)
            
            if m:
                if m.group(1):
                    tree[position] = m.group(1)
                else:
                    tree[position] = m.group(2)
        
            else:
                tree[position] = TagNonterminal(leaf)
        
        return tree.freeze()
    
    def as_tree(self, type=ImmutableTree):
        """the production as a regular tree. Interior nodes are converted to Nonterminal objects"""
        tree = Tree.convert(self)
        for subtree in tree.subtrees():
            if isinstance(subtree, Tree):
                subtree.node = Nonterminal(subtree.node.symbol())
                for i in range(len(subtree)):
                    subsubtree = subtree[i]
                    if isinstance(subsubtree, Nonterminal):
                        subtree[i] = Nonterminal(subsubtree.symbol())
        if type is Tree:
            return tree
        else:
            return type.convert(tree)
    
    @classmethod
    def _convert_cfg_rule(cls, rule):
        """helper method for construction a TagProduction out of a CFG Production object"""
        def convert_item(item):
            if type(item) is Nonterminal:
                return item.symbol()
            else:
                return "'%s'" % (item)
        
        return cls._convert_tree(Tree(convert_item(rule.lhs()), 
                                       [convert_item(item) 
                                        for item in rule.rhs()]))
            
    