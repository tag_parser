# This Python file uses the following encoding: utf-8
'''
Created on Apr 21, 2011

This is roughly based on the interface for nltk's ContextFreeGrammar

@author: mjacob
'''
from nltk.grammar import Nonterminal
from nltk.tree import Tree
import yaml
from collections import deque
from mjacob.annotations.memoized import Memoize
from mjacob.nltk.grammar.TagProduction import TagProduction
from mjacob.nltk.grammar.TagNonterminal import TagNonterminal
from functools import reduce

START = 'start'
PRODUCTIONS = 'productions'

class InvalidGrammarException(Exception): pass

class TreeAdjoiningGrammar(object):
    """class that represents a TAG grammar

A Tree Adjoining Grammar (TAG) is a tuple G = ⟨N, T, I, A, S, f,,OA,,, f,,SA,,⟩ where
 * N,T are disjoint alphabets of non-terminal and terminal symbols
 * S ∈ N is a specific start symbol
 * I is a finite set of initial trees, and A a finite set of auxiliary trees with node labels from N and T ∪ {ε}
 * f,,OA,, and f,,SA,, are functions that represent adjunction constraints:
  . f,,OA,, : {v|v vertex in some γ ∈ I ⋃ A} → {0,1} s.t. f,,OA,,(v) = 0 for every v with out-degree 0.
  . f,,SA,, : {v|v vertex in some γ ∈ I ⋃ A} → P(A) s.t. f,,SA,,(v) = ∅ for every v with out-degree 0.
 . Every _tree in I ∪ A is called an elementary _tree.
 . f,,OA,, specifies whether adjunction is obligatory at a node (1/0)
 . f,,SA,, gives the set of auxiliary trees that can be adjoined to a node. 
 . only internal nodes can allow for adjunction.
  . this specifically rules out leaves as adjunction sites

    """
    
    # some constants
    
    @classmethod
    def _convert_cfg(cls, cfg):
        """helper method for converting a CFG to a TAG"""
        productions = []
        for rule in cfg.productions():
            production = TagProduction(rule)
            productions.append(production)
        return {START: cfg.start(),
                PRODUCTIONS: frozenset(productions)}
        
    def __init__(self, 
                 filename=None, 
                 grammar=None, 
                 cfg=None, 
                 productions=None, 
                 start=None, 
                 loader=yaml.load):
        """create a new TAG from some specification, either a YAML string or file, or a CFG.
        The CFG is assumed to be an nltk CFG grammar, nltk.grammar.ContextFreeGrammar
        usage:
            
            TreeAdjoiningGrammar(filename=filename, loader=LOADER) # filename must be TAG grammar
                                                         # loader is an optional pickle loader (defaults to yaml)
            
            TreeAdjoiningGrammar(grammar=grammar, loader=LOADER) # grammar is string containing a TAG grammar
                                                       # loader is an optional pickle loader (defaults to yaml)
            
            TreeAdjoiningGrammar(cfg=cfg) # CFG is a NLTK ContextFreeGrammar
            
            TreeAdjoiningGrammar(productions=productions, start=start) # productions is a collection of TagProduction
                                                             # start is a Nonterminal
        
        """
        
        if len([x for x in (grammar, cfg, filename, productions) if x is not None]) != 1:
            raise ValueError("exactly 1 of grammar, cfg or filename must be specified")
        
        if productions is None:
            if filename or grammar:
                if filename:
                    with open(filename) as fh:
                        gram = loader(fh)
                elif grammar:
                    gram = loader(grammar)
                self._productions = frozenset(TagProduction(tree) for tree in gram[PRODUCTIONS])
                self._start = Nonterminal(gram[START]) # S
            else: # cfg
                gram = self._convert_cfg(cfg)
                self._productions = gram[PRODUCTIONS]
                self._start = gram[START] # S
            
        else:
            self._productions = frozenset(productions)
            self._start = start
        
        self.__lexical_productions_by_word = self._find_lexical_productions_by_word()
        self.__production_dependencies = self._find_production_dependencies()
        self.__epsilonic = self._find_epsilonic()

        self._precompute()
        self._validate()
    
    def _precompute(self):
        self.__filtered_productions = {}
        self._terminals = frozenset(self._find_terminals(self._productions))
        self._nonterminals = frozenset(self._find_nonterminals(self._productions)) # N
        
    def _find_epsilonic(self):
        epsilonic = []
        for production in self._productions:
            if production.is_initial() and production.is_nonlexical() and production.epsilon_treepositions():
                epsilonic.append(production)
        return frozenset(epsilonic)
        
    def save(self, filename=None, dumper=yaml.dump):
        """turn the current TAG into YAML, or optionally write it to a file. 
        
        useful for seeing what your TAG-ified CFGs look like."""
        yam = dumper({
                      START: self._start,
                      PRODUCTIONS: self._productions
                      })
        
        if filename:
            with open(filename, 'w') as fh:
                fh.write(yam)
                
        else:
            return yam
        
    def _find_terminals(self, productions):
        for production in productions:
            for leaf_position in production.leaf_treepositions():
                yield production[leaf_position]
    
    def _find_nonterminals(self, productions):
        for production in productions:
            for pos in production.treepositions():
                node = production.get_node(pos)
                if isinstance(node, TagNonterminal):
                    yield Nonterminal(node.symbol())
    
    def _find_production_dependencies(self):
        """for a given rule α, find all rules γ to which it may adjoin to or sub to."""
        deps_by_prod = {}
        
        for alpha in self._productions:
            label = alpha.root().symbol()
            deps = set()
        
            if alpha.is_auxiliary():
                for gamma in self._productions:
                    if gamma.adjunction_treepositions()[label]:
                        deps.add(gamma)
            
            else:
                for gamma in self._productions:
                    if gamma.substitution_treepositions()[label]:
                        deps.add(gamma)
            
            deps_by_prod[alpha] = frozenset(deps)
        
        return deps_by_prod

    def _find_lexical_productions_by_word(self):
        lexical_productions_by_word = {}
        for production in self._productions:
            for leaf in production.terminals():
                if leaf in lexical_productions_by_word:
                    lexical_productions_by_word[leaf].add(production)
                else:
                    lexical_productions_by_word[leaf] = set((production,))
        return lexical_productions_by_word
        
    def check_coverage(self, tokens):
        missing = [token for token in tokens if token not in self._terminals]
        if missing:
            raise ValueError("Grammar does not cover some of the "
                             "input words: %r." % missing)
        
    def _validate(self):
        """this method raises an exception if there is something grossly wrong with the specified grammar"""
        if len(self._terminals) == 0:
            raise InvalidGrammarException("there must be at least one terminal in the grammar")
        if len(self._nonterminals) == 0:
            raise InvalidGrammarException("there must be at least one non-terminal symbol in the grammar")
        if not self._start:
            raise InvalidGrammarException("there must be a start symbol in the grammar")
        
        initial_trees = False
        for production in self.productions(is_initial=True):
            if Nonterminal(production.root().symbol()) == self._start:
                initial_trees = True
            
            for pos, subtree in production.pos_subtrees():
                node = production.get_node(pos)
                
                if isinstance(subtree, Tree):
                    if not isinstance(node, TagNonterminal):
                        raise InvalidGrammarException('unexpected node found in non-terminal position: %s' % (subtree.node))
                    
                if isinstance(node, TagNonterminal):
                    if Nonterminal(node.symbol()) not in self._nonterminals:
                        raise InvalidGrammarException("unknown non-terminal '%s' found in _tree %s (should be in %s)" % (node.symbol(), production, self._nonterminals))
                    if node.is_foot():
                        raise InvalidGrammarException("foot nodes are not allowed in initial trees %s" % (production))
                
                else:
                    if node not in self._terminals:
                        raise InvalidGrammarException("unknown terminal '%s' found in _tree %s" % (node, production))
                    
        for production in self.productions(is_auxiliary=True):
            foot_node_count = 0
            
            for pos, subtree in production.pos_subtrees():
                node = production.get_node(pos)
                
                if isinstance(subtree, Tree):
                    if not isinstance(node, TagNonterminal):
                        raise InvalidGrammarException('unexpected node found in non-terminal position: %s' % (subtree.node))
                
                if isinstance(node, TagNonterminal):
                    if Nonterminal(node.symbol()) not in self._nonterminals:
                        raise InvalidGrammarException("unknown non-terminal '%s' found in _tree %s" % (node.symbol(), production))
                    
                    if node.is_foot():
                        if not isinstance(subtree, TagNonterminal):
                            raise InvalidGrammarException("foot node must be a leaf node!")
                        
                        if node.symbol() != production.root().symbol():
                            raise InvalidGrammarException("foot node label '%s' must be same as root node label %s" % (node.symbol(), production))
                        foot_node_count += 1
                else:
                    if node not in self._terminals:
                        raise InvalidGrammarException("unknown terminal '%s' found in _tree %s" % (node, production))
                
            if foot_node_count == 0:
                raise InvalidGrammarException("no foot node found in auxiliary _tree %s" % (production))
            
            if foot_node_count > 1:
                raise InvalidGrammarException("only one foot node is allowed in an auxiliary _tree %s" % (production))
        
        if initial_trees == 0:
            raise InvalidGrammarException("there are no starting trees in the given grammar")
        
    def _filter_production(self, production, **filters):
        for filter, value in list(filters.items()):
            if getattr(production, filter)() != value:
                return False
        return True    
    
    def _filter_productions(self, **filters):
        keys = tuple(sorted(filters))
        values = tuple(filters[key] for key in keys)
        if keys in self.__filtered_productions and values in self.__filtered_productions[keys]:
            return self.__filtered_productions[keys][values]

        filtered = tuple(production 
                         for production in self._productions
                         if self._filter_production(production, **filters))
        
        if not keys in self.__filtered_productions:
            self.__filtered_productions[keys] = {}
        self.__filtered_productions[keys][values] = filtered
        return filtered
    
    def terminals(self):
        return self._terminals
    
    def nonterminals(self):
        return self._nonterminals
    
    def productions(self, *roots, **filters):
        """returns an iterator of productions, given the specified root(s) and filter(s)"""
        
        if filters:
            to_iter = self._filter_productions(**filters)
        else:
            to_iter = self._productions
        
        if roots:
            def symbol(s):
                if isinstance(s, Nonterminal):
                    return s.symbol()
                else:
                    return s
            
            roots = frozenset(symbol(r) for r in roots)
                
            return (tree for tree in to_iter if tree.root().symbol() in roots)
        
        else:
            return (tree for tree in to_iter)
        
    def start(self):
        """
        @return: The start symbol of the grammar
        @rtype: L{Nonterminal}
        """
        return self._start
    
    @Memoize
    def is_lexical(self):
        """
        True if all productions are lexicalised.
        """
        return reduce(bool.__and__, (production.is_lexical() for production in self._productions))
        
    def is_nonlexical(self):
        """
        True if all lexical rules are "preterminals", that is,
        unary rules which can be separated in a preprocessing step.
        
        This means that all productions are of the forms
        A -> B1 ... Bn (n>=0), or A -> "s".
        
        Note: is_lexical() and is_nonlexical() are not opposites.
        There are grammars which are neither, and grammars which are both.
        """
        return reduce(bool.__and__, (production.is_nonlexical() for production in self._productions))
        
    def is_chomsky_normal_form(self):
        """
        A grammar is of Chomsky normal form if all productions
        are of the forms A -> B C, or A -> "s".
        """
        return reduce(bool.__and__, (production.is_chomsky_normal_form() for production in self._productions))
        
    def is_binarised(self):
        """
        True if all productions are at most binary.
        Note that there can still be empty and unary productions.
        """
        return reduce(bool.__and__, (production.is_binarised() for production in self._productions))
        
    def is_flexible_chomsky_normal_form(self):
        """
        True if all productions are of the forms
        A -> B C, A -> B, or A -> "s".
        """
        return reduce(bool.__and__, (production.is_flexible_chomsky_normal_form() for production in self._productions))
        
    def is_nonempty(self):
        """
        True if there are no empty productions.
        """
        return reduce(bool.__and__, (production.is_nonempty() for production in self._productions))
        
    def __repr__(self):
        return '<TreeAdjoiningGrammar with %d productions>' % len(self._productions)

    def __str__(self):
        return 'TreeAdjoiningGrammar with %d productions' % len(self._productions)
        
    def _is_subsequence(self, production, tokens):
        leaves = production.terminals()
        i = 0
        j = 0
        while i < len(leaves) and j < len(tokens):
            if leaves[i] == tokens[j]:
                i += 1
                j += 1
            else:
                j += 1
        return i == len(leaves)

    def reduce(self, tokens):
        productions = set()
        
        for production in self.__epsilonic:
            productions.add(production)
        
        missing = []
        for token in tokens:
            if not token in self.__lexical_productions_by_word:
                missing.append(token)
            else:
                for production in self.__lexical_productions_by_word[token]:
                    if production in productions:
                        continue;
                    
                    if self._is_subsequence(production, tokens):
                        productions.add(production)
        
        if missing:
            raise ValueError("Grammar does not cover some of the "
                             "input words: %r." % missing)
        
        fringe = deque(productions)
        while fringe:
            alpha = fringe.popleft()
            for gamma in self.__production_dependencies[alpha]:
                if gamma in productions:
                    continue
                else:
                    productions.add(gamma)
                    fringe.append(gamma)
        
        initial_rule_found = False
        for production in productions:
            if production.is_initial() and production.root().symbol() == self._start.symbol():
                initial_rule_found = True
                break
        if not initial_rule_found:
            productions = ()
        
        #print "%s productions reduced to %s" % (len(self._productions), len(productions))
        
        return SubTagGrammar(self._start, productions)    

class SubTagGrammar(TreeAdjoiningGrammar):
    def __init__(self, start, productions):
        self._start = start
        self._productions = productions

        self._precompute()   
