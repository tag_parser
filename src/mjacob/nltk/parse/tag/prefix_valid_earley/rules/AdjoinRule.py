# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

AdjoinRule: [Kallmeyer 2010 p. 95]
    [β,ε,ra,iβ,iβ,j,k,l,0]    EDGE
    [γ,p,rb,∼,j,g,h,k,0]      ADJOINED EDGE
    [γ,p,la,∼,∼,∼,∼,iβ,0]     BOUND EDGE
  → [γ,p,rb,∼,iβ,g,h,l,1]     NEW EDGE
    β ∈ fSA(γ,p)

@author: mjacob
'''
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import RA, RB, LA
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import CARENT,\
    PVTagEdge
from nltk.tree import Tree

class AdjoinTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, edge, adjoined_edge, bound_edge):
        if edge in working_on or adjoined_edge in working_on:
            return frozenset()
        
        old_trees = chart.get_trees(edge, working_on)
        pos = adjoined_edge.treeposition()
        
        if pos == ():
            return old_trees
        
        else:
            adjoined_trees = chart.get_trees(adjoined_edge, working_on)
            new_trees = []
            for adjoined_tree in adjoined_trees:
                tree = Tree.convert(adjoined_tree)
                
                for old_tree in old_trees:
                    tree[pos] = old_tree
                    new_trees.append(tree.freeze())
            
            return frozenset(new_trees)

class AbstractAdjoinRule(AbstractSingleEdgeTagChartRule):
    def _insert_new(self, chart, edge, adjoined_edge, bound_edge):
        new_edge = PVTagEdge(
                             adjoined_edge.production(), 
                             adjoined_edge.treeposition(),
                             CARENT, 
                             edge.span(), 
                             adjoined_edge.gap(), 
                             RB, 
                             True
                             )
        
        return new_edge, chart.insert(new_edge, AdjoinTreeBuilder(chart, type(self).__name__, edge, adjoined_edge, bound_edge))

class AdjoinRuleRight(AbstractAdjoinRule):
    def application_filter(self, chart, grammar):
        return {
                "is_auxiliary": True,
                "treeposition": (),
                "dotposition": RA,
                "has_gap": True,
                "startgap": 0
                }
    
    def apply_iter(self, chart, grammar, edge):
        for adjoined_edge in chart.select(
                                          adjoinable_symbol=edge.root_symbol(),
                                          dotposition=RB,
                                          span=edge.gap(),
                                          has_adjoined=False,
                                          ):
            for bound_edge in chart.select(
                                           dotposition=LA,
                                           productionid=adjoined_edge.productionid(),
                                           treeposition=adjoined_edge.treeposition(),
                                           end=edge.start(),
                                           ):
                new_edge, inserted = self._insert_new(chart, edge, adjoined_edge, bound_edge)
                if inserted:
                    yield new_edge

class AdjoinRuleCenter(AbstractAdjoinRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": RB,
                "has_adjoined": False,
                "can_adjoin": True,
                }
    
    def apply_iter(self, chart, grammar, adjoined_edge):
        for edge in chart.select(
                                 root_symbol=adjoined_edge.symbol(),
                                 is_auxiliary=True,
                                 treeposition=(),
                                 dotposition=RA,
                                 gap=adjoined_edge.span(),
                                 startgap=0,
                                 ):
            for bound_edge in chart.select(
                                           dotposition=LA,
                                           productionid=adjoined_edge.productionid(),
                                           treeposition=adjoined_edge.treeposition(),
                                           end=edge.start(),
                                           ):
                new_edge, inserted = self._insert_new(chart, edge, adjoined_edge, bound_edge)
                if inserted:
                    yield new_edge

class AdjoinRuleLeft(AbstractAdjoinRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "can_adjoin": True,
                }
    
    def apply_iter(self, chart, grammar, bound_edge):
        for edge in chart.select(
                                 root_symbol=bound_edge.symbol(),
                                 is_auxiliary=True,
                                 treeposition=(),
                                 dotposition=RA,
                                 has_gap=True,
                                 startgap=0,
                                 start=bound_edge.end(),
                                 ):
            for adjoined_edge in chart.select(
                                              productionid=bound_edge.productionid(),
                                              treeposition=bound_edge.treeposition(),
                                              dotposition=RB,
                                              has_adjoined=False,
                                              span=edge.gap(),
                                              ):
                new_edge, inserted = self._insert_new(chart, edge, adjoined_edge, bound_edge)
                if inserted:
                    yield new_edge
                    
                    