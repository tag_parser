# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

PredictAdjoinedRule: [Kallmeyer 2010 p. 95]
    [β,pf,la,iβ,i,−,−,m,0]    FOOT EDGE
    [γ,p,la,iγ,∼,∼,∼,iβ,0]    EDGE
  → [γ,p,lb,iγ,m,−,−,m,0]     NEW EDGE
    β(pf) foot node
    β ∈ fSA(γ,p)

@author: mjacob
'''
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import LA, LB
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge

class PredictAdjoinedTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, new_edge, edge, foot_edge):
        return frozenset((new_edge.production().as_tree(),))

class AbstractPredictAdjoinedRule(AbstractSingleEdgeTagChartRule):
    def _insert_new(self, chart, edge, foot_edge):
        new_edge = PVTagEdge(
                             edge.production(), 
                             edge.treeposition(), 
                             edge.treestart(),
                             (foot_edge.end(), foot_edge.end()), 
                             None, 
                             LB, 
                             False
                             )
        
        return new_edge, chart.insert(new_edge, PredictAdjoinedTreeBuilder(chart, type(self).__name__, new_edge, edge, foot_edge))

class PredictAdjoinedRuleRight(AbstractPredictAdjoinedRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "is_foot": True,
                "has_gap": False,
                }
    
    def apply_iter(self, chart, grammar, foot_edge):
        symbol = foot_edge.root_symbol()
        for edge in chart.select(
                                 symbol=symbol,
                                 can_adjoin=True,
                                 dotposition=LA,
                                 end=foot_edge.treestart(),
                                 ):
            
            new_edge, inserted = self._insert_new(chart, edge, foot_edge)
            if inserted:
                yield new_edge

class PredictAdjoinedRuleLeft(AbstractPredictAdjoinedRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "can_adjoin": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        symbol = edge.symbol()
        for foot_edge in chart.select(
                                      root_symbol=symbol,
                                      dotposition=LA,
                                      treestart=edge.end(),
                                      has_gap=False,
                                      is_foot=True,
                                      ):
            
            new_edge, inserted = self._insert_new(chart, edge, foot_edge)
            if inserted:
                yield new_edge

