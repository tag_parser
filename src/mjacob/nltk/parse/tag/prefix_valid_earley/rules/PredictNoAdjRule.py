# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

PredictNoAdjRule: [mine]
    [γ,p,la,iγ,i,j,k,l,0]
  → [γ,p,lb,iγ,l,-,-,l,0]
    fOA(γ,p) = 0

NOTE: this is reasonably different from the other earley PredictNoAdj rule

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge
from mjacob.nltk.parse.tag.earley.TagEdge import LB, LA

class PredictNoAdjRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "must_adjoin": False,
                "is_terminal": False
                }
    
    def apply_iter(self, chart, grammar, edge):
        new_edge = PVTagEdge(edge.production(), 
                             edge.treeposition(),
                             edge.treestart(),
                             (edge.end(), edge.end()), 
                             None, 
                             LB, 
                             False)
        if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
            yield new_edge