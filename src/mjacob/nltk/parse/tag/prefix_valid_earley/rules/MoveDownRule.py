# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

MoveDownRule: [Kallmeyer 2010 p. 95]
    [γ,p,lb,iγ,i,j,k,l,0]
  → [γ,p·1,la,iγ,i,j,k,l,0]

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import LA, LB
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder

class MoveDownRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LB,
                "has_children": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        new_edge = PVTagEdge(
                             edge.production(), 
                             edge.treeposition() + (0,),
                             edge.treestart(), 
                             edge.span(), 
                             edge.gap(),
                             LA,
                             False
                             )
        if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
            yield new_edge