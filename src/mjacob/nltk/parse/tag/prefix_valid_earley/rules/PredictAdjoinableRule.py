# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

PredictAdjoinableRule: [Kallmeyer 2010 p. 95]
    [γ,p,la,∼,∼,∼,∼,l,0]    
  → [β,ε,la,l,l,−,−,l,0]
    β ∈ fSA(γ,p)

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.TreeBuilderI import InitialTreeBuilder
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge
from mjacob.nltk.parse.tag.earley.TagEdge import LA

class PredictAdjoinableRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "can_adjoin": True,
                }
    
    def apply_iter(self, chart, grammar, edge):
        for production in grammar.productions(edge.symbol(), is_auxiliary=True):
            new_edge = PVTagEdge(production, (), edge.end(), (edge.end(), edge.end()), None, LA, False)
            if chart.insert(new_edge, InitialTreeBuilder(chart, type(self).__name__, new_edge)):
                yield new_edge
