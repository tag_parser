InitializeRule: p. 95
  → [α,ε,la,0,0,–,–,0,0]
    α ∈ I,l(α,ε) = S

ScanTermRule: p. 95
    [γ,p,la,iγ,i,j,k,l,0]
  → [γ,p,ra,i0,i,j,k,l + 1,0]
    l(γ,pγ·p) = wl+1 

ScanEpsilonRule: p. 95
    [γ,p,la,iγ,i,j,k,l,0]
  → [γ,p,ra,iγ,i,j,k,l,0]
    l(γ,pγ·p) = ε

ConvertRBRule: p. 95
    [γ,p,rb,∼,i,j,k,l,0]
  → [γ,p,rb,∼,i,∼,∼,l,0]

ConvertLA1Rule: p. 95
    [γ,p,la,iγ,i,j,k,l,0]
  → [γ,p,la,iγ,∼,∼,∼,l,0]

ConvertLA2Rule: p. 95
    [γ,p,la,iγ,i,j,k,l,0]
  → [γ,p,la,∼,∼,∼,∼,l,0]

PredictNoAdjRule: p. 95
    [γ,p,la,iγ,i,j,k,l,0]
  → [γ,p,lb,iγ,i,j,k,l,0]
    fOA(γ,p) = 0

PredictAdjoinableRule: p. 95
    [γ,p,la,∼,∼,∼,∼,l,0]    
  → [β,ε,la,l,l,−,−,l,0]
    β ∈ fSA(γ,p)

PredictAdjoinedRule: p. 95
    [β,pf,la,iβ,i,−,−,m,0]
    [γ,p,la,iγ,∼,∼,∼,iβ,0]
  → [γ,p,lb,iγ,m,−,−,m,0]
    β(pf) foot node
    β ∈ fSA(γ,p)

CompleteFootRule: p. 95
    [γ,p,rb,∼,i,∼,∼,l,0]
    [β,pf,la,iβ,m,–,–,i,0]
    [γ,p,la,∼,∼,∼,∼,iβ,0]
  → [β,pf,rb,∼,m,i,l,l,0]
    β(pf) foot node
    β ∈ fSA(γ,p)

AdjoinRule: p. 95
    [β,ε,ra,iβ,iβ,j,k,l,0]
    [γ,p,rb,∼,j,g,h,k,0]
    [γ,p,la,∼,∼,∼,∼,iβ,0]
  → [γ,p,rb,∼,iβ,g,h,l,1] 
    β ∈ fSA(γ,p)

CompleteNodeRule: p. 95
    [γ,p,la,iγ,f,g,h,i,0]
    [γ,p,rb,∼,i,j,k,l,adj]
  → [γ,p,ra,iγ,f,g⊕j,h⊕k,l,0]
    l(β,p) ∈ N

MoveDownRule: p. 95
    [γ,p,lb,iγ,i,j,k,l,0]
  → [γ,p·1,la,iγ,i,j,k,l,0]

MoveRightRule: p. 95
    [γ,p,ra,iγ,i,j,k,l,0]
  → [γ,p+1,la,iγ,i,j,k,l,0] 

MoveUpRule: p. 95
    [γ,p·m,ra,iγ,i,j,k,l,0]
  → [γ,p,rb,∼,i,j,k,l,0]
    γ(p·m+1) is not defined
    
PredictSubstitutedRule: p. 226
    [γ,p,la,∼,∼,∼,∼,i,0]
  → [α,ε,la,i,i,–,–,i,0]
    α ∈ I
    γ(p) substitution node
    l(γ,p) = l(α,ε)

SubstituteRule: p. 226
    [γ,p,la,∼,∼,∼,∼,i,0]
    [α,ε,ra,i,i,–,–,j,0]
  → [γ,p,rb,∼,i,–,–,j,0]
    α ∈ I
    γ(p) substitution node
    l(γ,p) = l(α,ε)