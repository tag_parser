# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

MoveUpRule: [Kallmeyer 2010 p. 95]
    [γ,p·m,ra,iγ,i,j,k,l,0]
  → [γ,p,rb,∼,i,j,k,l,0]
    γ(p·m+1) is not defined
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge,\
    CARENT
from mjacob.nltk.parse.tag.earley.TagEdge import RB, RA
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder

class MoveUpRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": RA,
                "has_sibling": False,
                "is_root": False
                }
    
    def apply_iter(self, chart, grammar, edge):
        oldpos = edge.treeposition()
        newpos = tuple(oldpos[:-1])
        new_edge = PVTagEdge(edge.production(), 
                             newpos,
                             CARENT, 
                             edge.span(), 
                             edge.gap(),
                             RB,
                             False
                             )
        if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
            yield new_edge