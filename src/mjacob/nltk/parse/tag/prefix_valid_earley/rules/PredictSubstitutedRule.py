# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

PredictSubstitutedRule: [Kallmeyer 2010 p. 226]
    [γ,p,la,∼,∼,∼,∼,i,0]
  → [α,ε,la,i,i,–,–,i,0]
    α ∈ I
    γ(p) substitution node
    l(γ,p) = l(α,ε)

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import LA
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge
from mjacob.nltk.parse.tag.TreeBuilderI import InitialTreeBuilder

class PredictSubstitutedRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "is_substitution_site": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        for production in grammar.productions(edge.symbol(), is_initial=True):
            new_edge = PVTagEdge(production, (), edge.end(), (edge.end(), edge.end()), None, LA, False)
            if chart.insert(new_edge, InitialTreeBuilder(chart, type(self).__name__, new_edge)):
                yield new_edge