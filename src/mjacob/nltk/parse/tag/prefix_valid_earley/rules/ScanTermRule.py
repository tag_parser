# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

ScanTermRule: [Kallmeyer 2010 p. 95]
    [γ,p,la,iγ,i,j,k,l,0]
  → [γ,p,ra,i0,i,j,k,l + 1,0]
    l(γ,pγ·p) = wl+1 
    
NOTE: what is i0? is it a typo of iγ????

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder
from mjacob.nltk.parse.tag.earley.TagEdge import LA, RA
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge

class ScanTermRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "is_terminal": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        if edge.end() < chart.num_leaves() and edge.terminal() == chart.leaf(edge.end()):
            new_edge = PVTagEdge(
                                 edge.production(), 
                                 edge.treeposition(), 
                                 edge.treestart(),
                                 (edge.start(), edge.end()+1), 
                                 edge.gap(), 
                                 RA, 
                                 False
                                 )
            if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
                yield new_edge