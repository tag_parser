# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

SubstituteRule: [Kallmeyer 2010 p. 226]
    [γ,p,la,∼,∼,∼,∼,i,0]    EDGE
    [α,ε,ra,i,i,–,–,j,0]    SUB_EDGE
  → [γ,p,rb,∼,i,–,–,j,0]    NEW EDGE
    α ∈ I
    γ(p) substitution node
    l(γ,p) = l(α,ε)

@author: mjacob
'''
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import RA, LA, RB
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge,\
    CARENT
from nltk.tree import Tree

class SubstituteTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, new_edge, edge, sub_edge):
        if sub_edge in working_on:
            return frozenset()
        
        production  = new_edge.production()
        pos = new_edge.treeposition()
        
        old_trees = chart.get_trees(sub_edge, working_on)
        new_trees = []

        tree = production.as_tree(type=Tree)
        
        for old_tree in old_trees:
            tree[pos] = old_tree
            new_trees.append(tree.freeze())
            
        return frozenset(new_trees)
        
class AbstractSubstituteRule(AbstractSingleEdgeTagChartRule):
    def _insert_new(self, chart, edge, sub_edge):
        new_edge = PVTagEdge(
                             edge.production(), 
                             edge.treeposition(), 
                             CARENT,
                             sub_edge.span(), 
                             None,
                             RB, 
                             False
                             )
        
        return new_edge, chart.insert(new_edge, SubstituteTreeBuilder(chart, type(self).__name__, new_edge, edge, sub_edge))

class SubstituteRuleRight(AbstractSubstituteRule):
    def application_filter(self, chart, grammar):
        return {
                "treeposition": (),
                "dotposition": RA,
                "has_gap": False,
                "is_initial": True,
                "startgap": 0
                }
    
    def apply_iter(self, chart, grammar, sub_edge):
        for edge in chart.select(
                                 symbol=sub_edge.root_symbol(),
                                 end=sub_edge.start(),
                                 dotposition=LA,
                                 is_substitution_site=True,
                                 ):
            
            new_edge, inserted = self._insert_new(chart, edge, sub_edge)
            if inserted:
                yield new_edge
    
class SubstituteRuleLeft(AbstractSubstituteRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "is_substitution_site": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        for sub_edge in chart.select(
                                     is_initial=True,
                                     root_symbol=edge.symbol(),
                                     startgap=0,
                                     has_gap=False,
                                     treeposition=(),
                                     start=edge.end(),
                                     dotposition=RA,
                                     ):
            
            new_edge, inserted = self._insert_new(chart, edge, sub_edge)
            if inserted:
                yield new_edge
    
    