# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

CompleteFootRule: [Kallmeyer 2010 p. 95]
    [γ,p,rb,∼,i,∼,∼,l,0]    EDGE
    [β,pf,la,iβ,m,–,–,i,0]  FOOT_EDGE
    [γ,p,la,∼,∼,∼,∼,iβ,0]   BOUND_EDGE
  → [β,pf,rb,∼,i,i,l,l,0]   NEW_EDGE
    β(pf) foot node
    β ∈ fSA(γ,p)

@author: mjacob
'''
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import RB, LA
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge,\
    CARENT
from nltk.tree import Tree

class CompleteFootTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, edge, foot_edge, bound_edge):
        if edge in working_on or foot_edge in working_on:
            return frozenset()
        
        oldpos = edge.treeposition()
        old_trees = [old_tree[oldpos] for old_tree in chart.get_trees(edge, working_on)]
        foot_trees = chart.get_trees(foot_edge, working_on)
        new_trees = []

        pos = foot_edge.treeposition()
        
        for foot_tree in foot_trees:
            tree = Tree.convert(foot_tree)
            
            for old_tree in old_trees:
                tree[pos] = old_tree
                new_trees.append(tree.freeze())
                
        return frozenset(new_trees)

class AbstractCompleteFootRule(AbstractSingleEdgeTagChartRule):
    def _insert_new(self, chart, edge, foot_edge, bound_edge):
        new_edge = PVTagEdge(
                             foot_edge.production(), 
                             foot_edge.treeposition(), 
                             CARENT,
                             (foot_edge.end(), edge.end()), 
                             edge.span(), 
                             RB, 
                             False
                             )
        
        return new_edge, chart.insert(new_edge, CompleteFootTreeBuilder(chart, type(self).__name__, edge, foot_edge, bound_edge))


class CompleteFootRuleRight(AbstractCompleteFootRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": RB,
                "has_adjoined": False,
                "can_adjoin": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        for foot_edge in chart.select(
                                      is_foot=True, 
                                      root_symbol=edge.symbol(),
                                      dotposition=LA,
                                      end=edge.start(),
                                      has_gap=False,
                                      ):
            for bound_edge in chart.select(
                                           productionid=edge.productionid(),
                                           treeposition=edge.treeposition(),
                                           dotposition=LA,
                                           end=foot_edge.treestart()
                                           ):
            
                new_edge, inserted = self._insert_new(chart, edge, foot_edge, bound_edge)
                if inserted:
                    yield new_edge

class CompleteFootRuleCenter(AbstractCompleteFootRule):
    def application_filter(self, chart, grammar):
        return {
                "is_foot": True,
                "dotposition": LA,
                "has_gap": False,
                }
    
    def apply_iter(self, chart, grammar, foot_edge):
        for edge in chart.select(
                                 dotposition=RB,
                                 has_adjoined=False,
                                 can_adjoin=True,
                                 symbol=foot_edge.root_symbol(),
                                 start=foot_edge.end(),
                                 ):
            for bound_edge in chart.select(
                                           productionid=edge.productionid(),
                                           treeposition=edge.treeposition(),
                                           dotposition=LA,
                                           end=foot_edge.treestart()
                                           ):
            
                new_edge, inserted = self._insert_new(chart, edge, foot_edge, bound_edge)
                if inserted:
                    yield new_edge

class CompleteFootRuleLeft(AbstractCompleteFootRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "can_adjoin": True
                }
    
    def apply_iter(self, chart, grammar, bound_edge):
        for edge in chart.select(
                                 dotposition=RB,
                                 has_adjoined=False,
                                 productionid=bound_edge.productionid(),
                                 treeposition=bound_edge.treeposition(),
                                 ):
            for foot_edge in chart.select(
                                          is_foot=True, 
                                          root_symbol=edge.symbol(),
                                          dotposition=LA,
                                          end=edge.start(),
                                          has_gap=False,
                                          treestart=bound_edge.end()
                                          ):
            
                new_edge, inserted = self._insert_new(chart, edge, foot_edge, bound_edge)
                if inserted:
                    yield new_edge

