# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

CompleteNodeRule: [Kallmeyer 2010 p. 95]
    [γ,p,la,iγ,f,g,h,i,0]
    [γ,p,rb,∼,i,j,k,l,adj]
  → [γ,p,ra,iγ,f,g⊕j,h⊕k,l,0]
    l(γ,p) ∈ N

@author: mjacob
'''
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from nltk.tree import Tree
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge
from mjacob.nltk.parse.tag.earley.TagEdge import RA, RB, LA

class CompleteNodeTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, edge, complete_edge):
        if edge in working_on or complete_edge in working_on:
            return frozenset()
        
        pos = edge.treeposition()
        if pos == ():
            return chart.get_trees(edge, working_on)
        
        old_trees = chart.get_trees(complete_edge, working_on)
        complete_trees = [complete_tree[pos] for complete_tree in chart.get_trees(edge, working_on)]
        
        new_trees = []
        for old_tree in old_trees:
            tree = Tree.convert(old_tree)
            
            for complete_tree in complete_trees:
                tree[pos] = complete_tree
                new_trees.append(tree.freeze())
                
        return frozenset(new_trees)
    
class AbstractCompleteNodeRule(AbstractSingleEdgeTagChartRule):
    def _insert_new(self, chart, gap, edge, complete_edge):
        new_edge = PVTagEdge(
                             edge.production(), 
                             edge.treeposition(), 
                             complete_edge.treestart(),
                             (complete_edge.start(), edge.end()), 
                             gap, 
                             RA, 
                             False
                             )
        
        return new_edge, chart.insert(new_edge, CompleteNodeTreeBuilder(chart, type(self).__name__, edge, complete_edge))

class CompleteNodeRuleRight(AbstractCompleteNodeRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition":RB,
                }
    
    def apply_iter(self, chart, grammar, edge):
        select = {
                  "productionid":edge.productionid(),
                  "treeposition":edge.treeposition(),
                  "end":edge.start(),
                  "dotposition": LA,
                  }
            
        if edge.has_gap():
            select["has_gap"] = False
            gap = edge.gap()
        else:
            gap = None
        
        for complete_edge in chart.select(**select):
            if not gap:
                gap = complete_edge.gap() 

            new_edge, inserted = self._insert_new(chart, gap, edge, complete_edge)
            if inserted:
                yield new_edge

class CompleteNodeRuleLeft(AbstractCompleteNodeRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "is_nonterminal": True,
                }
    
    def apply_iter(self, chart, grammar, complete_edge):
        select = {
                  "productionid":complete_edge.productionid(),
                  "treeposition":complete_edge.treeposition(),
                  "dotposition":RB,
                  "start":complete_edge.end(),
                  }
            
        if complete_edge.has_gap():
            select["has_gap"] = False
            gap = complete_edge.gap()
        else:
            gap = None
        
        for edge in chart.select(**select):
            if not gap:
                gap = edge.gap()

            new_edge, inserted = self._insert_new(chart, gap, edge, complete_edge)
            if inserted:
                yield new_edge
                
                