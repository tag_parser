# This Python file uses the following encoding: utf-8
'''
Created on June 17th, 2011

@author: mjacob
'''
from mjacob.nltk.parse.tag.strategies import Strategy
from mjacob.nltk.parse.tag.earley.TagEdge import RA
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.AdjoinRule import AdjoinRuleCenter,\
    AdjoinRuleLeft, AdjoinRuleRight
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.CompleteFootRule import CompleteFootRuleRight,\
    CompleteFootRuleCenter, CompleteFootRuleLeft
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.InitializeRule import InitializeRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.MoveDownRule import MoveDownRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.MoveRightRule import MoveRightRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.MoveUpRule import MoveUpRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.PredictAdjoinableRule import PredictAdjoinableRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.PredictAdjoinedRule import PredictAdjoinedRuleRight,\
    PredictAdjoinedRuleLeft
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.PredictNoAdjRule import PredictNoAdjRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.PredictSubstitutedRule import PredictSubstitutedRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.ScanEpsilonRule import ScanEpsilonRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.ScanTermRule import ScanTermRule
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.SubstituteRule import SubstituteRuleRight,\
    SubstituteRuleLeft
from mjacob.nltk.parse.tag.prefix_valid_earley.PVTagEdge import PVTagEdge
from mjacob.nltk.parse.tag.prefix_valid_earley.rules.CompleteNodeRule import CompleteNodeRuleLeft,\
    CompleteNodeRuleRight

class TagPrefixValidEarleyStrategy(Strategy):
    def goal_edges(self, chart, symbol):
        """ p. 94
        [α,0,ra,0,0,−,−,n,0], 
        α ∈ I,l(α,ε) = S
        """
        return chart.select(
                            treestart=0,
                            start=0, 
                            end=chart.num_leaves(), 
                            has_gap=False, 
                            treeposition=(),
                            dotposition=RA,
                            is_initial=True,
                            root_symbol=symbol
                            )

TAG_PREFIX_VALID_EARLEY_STRATEGY = TagPrefixValidEarleyStrategy(
                                                                PVTagEdge
                                                              , AdjoinRuleRight()
                                                              , CompleteFootRuleRight()
                                                              , CompleteNodeRuleRight()
                                                              , CompleteNodeRuleLeft()
                                                              , InitializeRule()
                                                              , MoveDownRule()
                                                              , MoveRightRule()
                                                              , MoveUpRule()
                                                              , PredictAdjoinableRule()
                                                              , PredictAdjoinedRuleRight()
                                                              , PredictNoAdjRule()
                                                              , PredictSubstitutedRule()
                                                              , ScanEpsilonRule()
                                                              , ScanTermRule()
                                                              , SubstituteRuleRight()
                                                              , SubstituteRuleLeft()
                                                                )