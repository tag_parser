# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011

Based on nltk's EdgeI

@author: mjacob
'''
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge
from operator import lt, eq

"""the symbol for an index whose value doesn't matter is ~"""
CARENT="~"

class PVTagEdge(TagEdge):
    def __init__(self, production, treeposition, treestart, span, gapspan, dotposition, has_adjoined):
        TagEdge.__init__(self, production, treeposition, span, gapspan, dotposition, has_adjoined)
        self.__treestart = treestart
    
    def treestart(self):
        """the start position of the elementary tree that has triggered the creation of this edge"""
        return self.__treestart
    
    def startgap(self):
        if self.__treestart == CARENT:
            return None
        return self.start() - self.__treestart

    def __str__(self): 
        return '[%s:%s:%s:%s:%s] %r %s[%s] %s %s' % (self.__treestart,
                                                     self.start(), 
                                                     self.gapstart() is None and "-" or self.gapstart(), 
                                                     self.gapend() is None and "-" or self.gapend(), 
                                                     self.end(), 
                                                     self.text(),
                                                     self.production(), 
                                                     self.treeposition(),
                                                     self.dotposition(),
                                                     self.has_adjoined())
    
    def __repr__(self):
        return '[Edge: %s]' % (self)

    def __lt__(self, othr):
        return lt((self.__treestart, self.start(), self.gapstart(), self.gapend(), self.end(), self.production(), self.treeposition(), self.dotposition(), self.has_adjoined()),
                  (othr.__treestart, othr.start(), othr.gapstart(), othr.gapend(), othr.end(), othr.production(), othr.treeposition(), othr.dotposition(), othr.has_adjoined()))

    def __eq__(self, othr):
        return eq((self.__treestart, self.start(), self.gapstart(), self.gapend(), self.end(), self.production(), self.treeposition(), self.dotposition(), self.has_adjoined()),
                  (othr.__treestart, othr.start(), othr.gapstart(), othr.gapend(), othr.end(), othr.production(), othr.treeposition(), othr.dotposition(), othr.has_adjoined()))

    def __hash__(self):
        return hash((self.__treestart, self.production(), self.treeposition(), self.span(), self.gap(), self.dotposition(), self.has_adjoined()))
