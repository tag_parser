# This Python file uses the following encoding: utf-8
'''
Created on May 30, 2011

@author: mjacob
'''
from abc import abstractmethod

class Strategy(tuple, metaclass=ABCMeta):
    """represents a ChartParser strategy"""

    def __new__(cls, edge_class, *rules):
        return tuple.__new__(cls, rules)
    
    def __init__(self, edge_class, *rules):
        super(Strategy, self).__init__(rules)
        self.__edge_class = edge_class
    
    def edge_class(self):
        return self.__edge_class
    
    @abstractmethod
    def goal_edges(self, chart, symbol): pass
    
    def goal_found(self, chart, symbol):
        try:
            next(self.goal_edges(chart, symbol))
            return True
        except StopIteration:
            return False