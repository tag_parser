# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011

This is a hacked version of what's in NLTK 
(approximate) copyright below.

@author: mjacob
'''
# -*- coding: utf-8 -*-
# Natural Language Toolkit: A Chart Parser
#
# Copyright (C) 2001-2010 NLTK Project
# Author: Edward Loper <edloper@gradient.cis.upenn.edu>
#         Steven Bird <sb@csse.unimelb.edu.au>
#         Jean Mark Gawron <gawron@mail.sdsu.edu>
#         Peter Ljunglöf <peter.ljunglof@heatherleaf.se>
# URL: <http://www.nltk.org/>
# For license information, see LICENSE.TXT
#
# Id: chart.py 8506 2010-03-08 22:16:33Z peter.ljunglof@heatherleaf.se 
from nltk.tree import ImmutableTree
from nltk.parse.api import ParserI
from nltk.grammar import ContextFreeGrammar
from pydoc import deque
from mjacob.nltk.parse.tag.earley.rules import TAG_EARLEY_STRATEGY
from mjacob.nltk.parse.tag.TagChart import TagChart
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar
from mjacob.nltk.parse.tag.prefix_valid_earley.rules import TAG_PREFIX_VALID_EARLEY_STRATEGY

class TagChartParser(ParserI):
    """
    A generic chart parser.  A X{strategy}, or list of
    L{ChartRules<ChartRuleI>}, is used to decide what edges to add to
    the chart.  In particular, C{ChartParser} uses the following
    algorithm to parse texts:

        - Until no new edges are added:
          - For each I{rule} in I{strategy}:
            - Apply I{rule} to any applicable edges in the chart.
        - Return any complete parses in the chart
    """
    def __init__(self, grammar, 
                 strategy=TAG_EARLEY_STRATEGY, 
                 use_agenda=True, 
                 chart_class=TagChart):
        """
        Create a new chart parser, that uses C{grammar} to parse
        texts.

        @type grammar: L{ContextFreeGrammar}
        @param grammar: The grammar used to parse texts.
        @type strategy: C{list} of L{ChartRuleI}
        @param strategy: A list of rules that should be used to decide
            what edges to add to the chart (top-down strategy by default).
        @type use_agenda: C{bool}
        @param use_agenda: Use an optimized agenda-based algorithm, 
            if possible. 
        @param chart_class: The class that should be used to create
            the parse charts.
        """
        if isinstance(grammar, ContextFreeGrammar):
            self.__grammar = TreeAdjoiningGrammar(cfg=grammar)
        else:
            self.__grammar = grammar
            
        self.__strategy = strategy
        self.__chart_class = chart_class
        
        self.__use_agenda = use_agenda
        if use_agenda:
            # If the strategy only consists of axioms (NUM_EDGES==0) and
            # inference rules (NUM_EDGES==1), we can use an agenda-based algorithm:
            axioms = []
            inference_rules = []
            for rule in strategy:
                if rule.NUM_EDGES == 0:
                    axioms.append(rule)
                elif rule.NUM_EDGES == 1:
                    inference_rules.append(rule)
                else:
                    self.__use_agenda = False
                    break
            
            if self.__use_agenda:
                self.__axioms = axioms
                self.__inference_rules = inference_rules

    def grammar(self):
        return self.__grammar

    def _parse_with_agenda(self, tokens, chart, grammar):
        
        for axiom in self.__axioms:
            axiom.apply(chart, grammar)
        
        agenda = deque(chart.edges())
        
        while agenda:
            edge = agenda.popleft()
            #print " NEXT EDGE: %s" % (edge)
            for rule in self.__inference_rules:
                if not rule.applies_to(chart, grammar, edge): continue
                
                #print "  NEXT RULE: %s" % (rule)
                agenda.extend(rule.apply_iter(chart, grammar, edge))
        

    def chart_parse(self, tokens):
        """
        @return: The final parse L{Chart}, 
        from which all possible parse trees can be extracted.
        
        @param tokens: The sentence to be parsed
        @type tokens: L{list} of L{string}
        @rtype: L{Chart}
        """
        tokens = list(tokens)
        chart = self.__chart_class(tokens, self.__strategy.edge_class())
        grammar = self.__grammar.reduce(tokens)
        grammar.check_coverage(tokens)
        
        if self.__use_agenda:
            # Use an agenda-based algorithm.
            self._parse_with_agenda(tokens, chart, grammar)
        
        else:
            # Do not use an agenda-based algorithm.
            self._parse_without_agenda(tokens, chart, grammar)

        # Return the final chart.
        return chart
    
    def _parse_without_agenda(self, tokens, chart, grammar):
        edges_added = True
        while edges_added:
            edges_added = False
            for rule in self.__strategy:
                new_edges = rule.apply_everywhere(chart, grammar)
                edges_added = len(new_edges)

    def accept(self, tokens):
        chart = self.chart_parse(tokens)
        return chart.accept(self.__grammar.start().symbol(), self.__strategy)

    def nbest_parse(self, tokens, n=None, tree_class=ImmutableTree):
        chart = self.chart_parse(tokens)
        # Return a list of complete parses.
        if n is not None:
            return chart.parses(self.__grammar.start().symbol(), self.__strategy, tree_class=tree_class)[:n]
        else:
            return chart.parses(self.__grammar.start().symbol(), self.__strategy, tree_class=tree_class)

class PrefixValidTagChartParser(TagChartParser):
    def __init__(self, 
                 grammar, 
                 use_agenda=True, 
                 chart_class=TagChart):
        TagChartParser.__init__(self, 
                                grammar, 
                                strategy=TAG_PREFIX_VALID_EARLEY_STRATEGY,
                                use_agenda=use_agenda,
                                chart_class=TagChart)

