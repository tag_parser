# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011

Based on nltk's EdgeI

@author: mjacob
'''
from nltk.tree import Tree
from nltk.grammar import Nonterminal
from mjacob.annotations.memoized import Memoize
from mjacob.nltk.grammar.TagNonterminal import TagNonterminal
from operator import lt, eq

"""dot positions: """
LA = "LA"
LB = "LB"
RA = "RA"
RB = "RB"

class TagEdge(object):
    """
    A hypothesis about the structure of part of a sentence.
    Each edge records the fact that a structure is (partially)
    consistent with the sentence.  An edge contains:

        - A X{span}, indicating what part of the sentence is
          consistent with the hypothesized structure.

        - A X{gap}, indicating a hypothesis about the possible 
          contents of a foot node
        
        - A X{treeposition}, indicating which portion of a
          TagProduction we are analyzing
        
        - A X{dotposition}, indicating analysis of a treeposition
          w.r.t. adjunction
          
        - X{has_adjoined}, which indicates whether adjunction has already occurred
          at the indicated node

    There are two kinds of edge:

        - C{TreeEdges<TagTreeEdge>} record which trees have been found to
          be (partially) consistent with the text.
          
        - C{LeafEdges<TagLeafEdge>} record the tokens occur in the text.

    The C{TagEdgeI} interface provides a common interface to both types
    of edge, allowing chart parsers to treat them in a uniform manner.
    """

    def __init__(self, production, treeposition, span, gapspan, dotposition, has_adjoined):
        self.__production = production
        self.__treeposition = treeposition
        self.__span = span
        self.__gapspan = gapspan
        self.__dotposition = dotposition
        self.__has_adjoined = has_adjoined

    def is_auxiliary(self):
        """
        True iff the edge's production is auxiliary
        """
        return self.__production.is_auxiliary()
        
    def is_initial(self):
        """
        True iff the edge's production is initial
        """
        return self.__production.is_initial()
        
    #////////////////////////////////////////////////////////////
    # Span
    #////////////////////////////////////////////////////////////

    def span(self):
        """
        @return: A tuple C{(s,e)}, where C{subtokens[s:e]} is the
            portion of the sentence that is consistent with this
            edge's structure.
        @rtype: C{(int, int)}
        """
        return self.__span
        
    def start(self):
        """
        @return: The start index of this edge's span.
        @rtype: C{int}
        """
        return self.__span[0]

    def end(self):
        """
        @return: The end index of this edge's span.
        @rtype: C{int}
        """
        return self.__span[1]

    def length(self):
        """
        @return: The length of this edge's span.
        @rtype: C{int}
        """
        return self.__span[1] - self.__span[0]

    def has_gap(self):
        """
        True iff there is a gap (foot of an auxiliary node) with unfilled content
        """
        return self.__gapspan is not None

    def gap(self):
        """
        @return: A tuple C{(s,e)}, where C{subtokens[s:e]} represents
            a potential gap in the structure due to an auxiliary foot node 
        @rtype: C{(int, int)}
        """
        return self.__gapspan

    def gapstart(self):
        """
        the start of the gap
        """
        if self.__gapspan is not None:
            return self.__gapspan[0]
        else:
            return None 
        
    def gapend(self):
        """
        the end of the gap
        """
        if self.__gapspan is not None:
            return self.__gapspan[1]
        else:
            return None 
        
    def gaplength(self):
        """
        the length of the gap
        """
        if self.__gapspan is not None:
            return self.__gapspan[1] - self.__gapspan[0] 
        
    #////////////////////////////////////////////////////////////
    # Left Hand Side
    #////////////////////////////////////////////////////////////

    def production(self): 
        """the production associated with this edge"""
        return self.__production
    
    def productionid(self):
        """
        the id of the production associated with this edge.
        this is a useful optimization for selecting edges by production
        without having to do a full tree comparison.
        """
        return id(self.__production)
    
    @Memoize
    def _node(self):
        """the node of the production selected by the edge's treeposition"""
        return self.__production.get_node(self.__treeposition)
    
    @Memoize
    def _subtree(self):
        """the subtree of the production selected by the edge's treeposition"""
        return self.__production[self.__treeposition]

    def has_adjoined(self): 
        """
        True iff the edge represents an adjunction of the production @ treeposition
        """
        return self.__has_adjoined

    def can_adjoin(self):
        """
        True iff adjunction is permissible at production[treeposition]
        substitution (and foot?) positions can never be adjunction positions.
        """
        subtree = self._subtree()
        return isinstance(subtree, Tree) and not subtree.node.no_adjunction()
        
    def adjoinable_symbol(self):
        """if adjunction is permissible at production[treeposition], return
        the symbol which may be adjoined. otherwise, return None"""
        node = self._node()
        if (isinstance(node, TagNonterminal) and not node.no_adjunction()):
            return node.symbol()
        else:
            return None
        
    def must_adjoin(self):
        """True iff adjunction is obligatory at production[treeposition]"""
        node = self._node()
        return isinstance(node, TagNonterminal) and node.obligatory_adjunction()
            
    def root_symbol(self):
        """
        @return: This edge's root, which specifies what kind
            of structure is hypothesized by this edge.
        @see: L{TagTreeEdge} and L{TagLeafEdge} for a description of
            the root values for each edge type.
        """
        return self.__production.node.symbol()
        
    def symbol(self):
        """
        @return: This edge's node, which specifies what kind
            of structure is hypothesized by this edge.
        @see: L{TagTreeEdge} and L{TagLeafEdge} for a description of
            the root values for each edge type.
        """
        node = self._node()
        if isinstance(node, Nonterminal):
            return node.symbol()
        else:
            return None
        
    def terminal(self):
        """True iff production[treeposition] is a terminal element"""
        node = self._node()
        if isinstance(node, str):
            return node
        else:
            return None
    
    def text(self):
        """if production[treeposition] is non-terminal, the symbol of the non-terminal.
        otherwise, the text of the terminal"""
        node =self._node()
        if isinstance(node, Nonterminal):
            return node.symbol()
        else:
            return node
        
    def is_substitution_site(self):
        """
        True iff substitution is allowed at production[treeposition]
        """
        subtree = self._subtree()
        return isinstance(subtree, TagNonterminal) and not subtree.is_foot()
        
    def is_foot(self):
        """
        True iff production[treeposition] is the foot of an auxiliary
        """
        node = self._node()
        return isinstance(node, TagNonterminal) and node.is_foot()
    
    def is_terminal(self):
        """
        True iff production[treeposition] is a terminal
        """
        node = self._node()
        return isinstance(node, str)
    
    def is_epsilon(self):
        """
        True iff production[treeposition] is an internal, non-terminal node with no children.
        """
        subtree = self._subtree()
        return isinstance(subtree, Tree) and len(subtree) == 0
        
    def is_nonterminal(self):
        """
        True iff production[treeposition] is non-terminal
        """
        node = self._node()
        return isinstance(node, Nonterminal)
    
    #////////////////////////////////////////////////////////////
    # Right Hand Side
    #////////////////////////////////////////////////////////////

    def treeposition(self): 
        """
        @return: This edge's tree position, which indicates how much of
            the hypothesized structure is consistent with the
            sentence.  In particular, C{self.production[treeposition]} is consistent
            with C{subtokens[self.start():self.end()]}.
        @rtype: C{list(int)}
        """
        return self.__treeposition
        
    def dotposition(self): 
        """
        returns the dot status of the edge
        """
        return self.__dotposition
        
    def is_dot_left(self):
        """
        returns true if the dot is on the left of the node
        """
        return self.__dotposition in (LA, LB)
    
    def is_dot_right(self):
        """
        returns True iff the dot is on the right of the node
        """
        return self.__dotposition in (RA, RB)
    
    def is_dot_above(self):
        """
        returns True iff the dot is above the node
        """
        return self.__dotposition in (LA, RA)
    
    def is_dot_below(self):
        """
        returns True iff the dot is below the node
        """
        return self.__dotposition in (LB, RB)
    
    @Memoize    
    def has_sibling(self):
        """
        return True iff there is a node to the right of treeposition
        """
        pos = self.__treeposition
        return (pos != ()
            and len(self.__production[pos[:-1]]) > pos[-1]+1
                )
    
    def has_children(self):
        """returns True iff treeposition has children"""
        subtree = self._subtree()
        return isinstance(subtree, Tree) and len(subtree) > 0 
    
    def is_root(self):
        """returns True iff treeposition is the root of the tree"""
        return self.__treeposition == () 
        
    def child_nodes(self):
        """returns a tuple of the nodes below production[treeposition]"""
        subtree = self._subtree()
        if isinstance(subtree, Tree):
            return tuple(subsubtree.node.symbol() for subsubtree in subtree)
        else:
            return ()

    #////////////////////////////////////////////////////////////
    # Comparisons
    #////////////////////////////////////////////////////////////
    def __str__(self): 
        return '[%s:%s:%s:%s] %r %s[%s] %s %s' % (self.__span[0], 
                                                  self.__gapspan is None and "-" or self.__gapspan[0], 
                                                  self.__gapspan is None and "-" or self.__gapspan[1], 
                                                  self.__span[1], 
                                                  self.text(),
                                                  self.__production, 
                                                  self.__treeposition,
                                                  self.__dotposition,
                                                  self.__has_adjoined)
    
    def __repr__(self):
        return '[Edge: %s]' % (self)

    def __lt__(self, othr):
        return lt((self.start(), self.gapstart(), self.gapend(), self.end(), self.__production, self.__treeposition, self.__dotposition, self.__has_adjoined),
                  (othr.start(), othr.gapstart(), othr.gapend(), othr.end(), othr.__production, othr.__treeposition, othr.__dotposition, othr.__has_adjoined))

    def __eq__(self, othr):
        return eq((self.start(), self.gapstart(), self.gapend(), self.end(), self.__production, self.__treeposition, self.__dotposition, self.__has_adjoined),
                  (othr.start(), othr.gapstart(), othr.gapend(), othr.end(), othr.__production, othr.__treeposition, othr.__dotposition, othr.__has_adjoined))

    def __hash__(self):
        return hash((self.__production, self.__treeposition, self.__span, self.__gapspan, self.__dotposition, self.__has_adjoined))
    
