# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.86]
    
    [γ,p,la,i,j,k,l,0]
  → [β,0,la,l,−,−,l,0]  β ∈ fSA(γ, p)
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LA
from mjacob.nltk.parse.tag.TreeBuilderI import InitialTreeBuilder

class PredictAdjoinableRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "can_adjoin": True,
                }
    
    def apply_iter(self, chart, grammar, edge):
        for production in grammar.productions(edge.symbol(), is_auxiliary=True):
            new_edge = TagEdge(production, (), (edge.end(), edge.end()), None, LA, False)
            if chart.insert(new_edge, InitialTreeBuilder(chart, type(self).__name__, new_edge)):
                yield new_edge
            