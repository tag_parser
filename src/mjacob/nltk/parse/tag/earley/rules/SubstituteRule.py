# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.90]
    
    [α,ε,ra,i,–,–,j,0]
  → [γ,p,rb,i,–,–,j,0] γ(p) a substitution node, α ∈ I,l(γ,p) = l(α,ε)


@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, RA, RB
from nltk.tree import Tree
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI

class SubstituteTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, edge, new_edge):
        if edge in working_on:
            return frozenset()
        
        production  = new_edge.production()
        pos = new_edge.treeposition()
        
        old_trees = chart.get_trees(edge, working_on)
        new_trees = []

        tree = production.as_tree(type=Tree)
        
        for old_tree in old_trees:
            tree[pos] = old_tree
            new_trees.append(tree.freeze())
            
        return frozenset(new_trees)
        
class SubstituteRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "treeposition": (),
                "dotposition": RA,
                "has_gap": False,
                "is_initial": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        for production in grammar.productions():
            for sub_pos in production.substitution_treepositions()[edge.root_symbol()]:
                new_edge = TagEdge(production, sub_pos, edge.span(), None, RB, False)
                if chart.insert(new_edge, SubstituteTreeBuilder(chart, type(self).__name__, edge, new_edge)):
                    yield new_edge
            
