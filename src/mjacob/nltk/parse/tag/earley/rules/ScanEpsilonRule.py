# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.85]
    
    [γ,p,la,i,j,k,l,0]
  → [γ,p,ra,i,j,k,l,0]  l(γ,p) = ε
  
    this rule is modified somewhat; instead of having "epsilon" productions,
    my implementation just has internal nodes which have no children.
    thus, instead of scanning over an "epsilon", we scan under an empty production.
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LB, RB
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder

class ScanEpsilonRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LB,
                "is_epsilon": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        new_edge = TagEdge(edge.production(), edge.treeposition(), edge.span(), edge.gap(), RB, False)
        if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
            yield new_edge
            

