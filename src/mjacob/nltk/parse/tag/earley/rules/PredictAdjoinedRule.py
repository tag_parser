# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.86]
    
    [β,pf,lb,l,−,−,l,0]
  → [γ,p,lb,l,−,−,l,0]  pf is the foot node address in β, β ∈ fSA(γ,p)
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LB
from mjacob.nltk.parse.tag.TreeBuilderI import InitialTreeBuilder

class PredictAdjoinedRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "is_foot": True,
                "dotposition": LB,
                "has_gap": False,
                "length": 0
                }
    
    def apply_iter(self, chart, grammar, edge):
        for production in grammar.productions():
            for treeposition in production.treepositions():
                if edge.production().can_adjoin(production, treeposition):
                    new_edge = TagEdge(production, treeposition, edge.span(), None, LB, False)
                    if chart.insert(new_edge, InitialTreeBuilder(chart, type(self).__name__, new_edge)):
                        yield new_edge