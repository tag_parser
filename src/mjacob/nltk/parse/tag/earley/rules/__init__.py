# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

@author: mjacob
'''
from mjacob.nltk.parse.tag.strategies import Strategy
from mjacob.nltk.parse.tag.earley.rules.AdjoinRule import AdjoinRuleRight,\
    AdjoinRuleLeft
from mjacob.nltk.parse.tag.earley.rules.CompleteFootRule import CompleteFootRuleRight,\
    CompleteFootRuleLeft
from mjacob.nltk.parse.tag.earley.rules.InitializeRule import InitializeRule
from mjacob.nltk.parse.tag.earley.rules.MoveDownRule import MoveDownRule
from mjacob.nltk.parse.tag.earley.rules.MoveRightRule import MoveRightRule
from mjacob.nltk.parse.tag.earley.rules.MoveUpRule import MoveUpRule
from mjacob.nltk.parse.tag.earley.rules.PredictAdjoinableRule import PredictAdjoinableRule
from mjacob.nltk.parse.tag.earley.rules.PredictAdjoinedRule import PredictAdjoinedRule
from mjacob.nltk.parse.tag.earley.rules.PredictNoAdjRule import PredictNoAdjRule
from mjacob.nltk.parse.tag.earley.rules.PredictSubstituteRule import PredictSubstituteRule
from mjacob.nltk.parse.tag.earley.rules.ScanEpsilonRule import ScanEpsilonRule
from mjacob.nltk.parse.tag.earley.rules.ScanTermRule import ScanTermRule
from mjacob.nltk.parse.tag.earley.rules.SubstituteRule import SubstituteRule
from mjacob.nltk.parse.tag.earley.TagEdge import RA, TagEdge
from mjacob.nltk.parse.tag.earley.rules.CompleteNodeRule import CompleteNodeRuleRight,\
    CompleteNodeRuleLeft

class TagEarleyStrategy(Strategy):
    def goal_edges(self, chart, symbol):
        return chart.select(
                            start=0, 
                            end=chart.num_leaves(), 
                            has_gap=False, 
                            treeposition=(),
                            dotposition=RA,
                            is_initial=True,
                            root_symbol=symbol
                            )

TAG_EARLEY_STRATEGY = TagEarleyStrategy(
                                        TagEdge
                                      , AdjoinRuleRight()
                                      , CompleteFootRuleRight()
                                      , CompleteNodeRuleRight()
                                      , CompleteNodeRuleLeft()
                                      , InitializeRule()
                                      , MoveDownRule()
                                      , MoveRightRule()
                                      , MoveUpRule()
                                      , PredictAdjoinableRule()
                                      , PredictAdjoinedRule()
                                      , PredictNoAdjRule()
                                      , PredictSubstituteRule()
                                      , ScanEpsilonRule()
                                      , ScanTermRule()
                                      , SubstituteRule()
                                       )
