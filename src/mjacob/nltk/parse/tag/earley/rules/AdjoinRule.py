# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.87]
    
    [β,ε,ra,i,j,k,l,0]  β ∈ fSA(γ,p)
    [γ,p,rb,j,g,h,k,0]
  → [γ,p,rb,i,g,h,l,1]
    
    ex:
    [3:4:5:6] S (S a (X *S b))[()] RA False "EDGE"
    [4:-:-:5] S (S (X d f))[()] RB False    "ADJOINED EDGE"
  → [3:-:-:6] S (S (X d f))[()] RB True

    new tree: (S a (X (S (X d f)) b))

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import RA, RB, TagEdge
from nltk.tree import Tree
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI

class AdjoinTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, edge, adjoined_edge):
        if edge in working_on or adjoined_edge in working_on:
            return frozenset()
        
        old_trees = chart.get_trees(edge, working_on)
        pos = adjoined_edge.treeposition()
        
        if pos == ():
            return old_trees
        
        else:
            adjoined_trees = chart.get_trees(adjoined_edge, working_on)
            new_trees = []
            for adjoined_tree in adjoined_trees:
                tree = Tree.convert(adjoined_tree)
                
                for old_tree in old_trees:
                    tree[pos] = old_tree
                    new_trees.append(tree.freeze())
            
            return frozenset(new_trees)

class AbstractAdjoinRule(AbstractSingleEdgeTagChartRule):
    def _insert_new(self, chart, edge, adjoined_edge):
        new_edge = TagEdge(
                           adjoined_edge.production(), 
                           adjoined_edge.treeposition(), 
                           edge.span(), 
                           adjoined_edge.gap(), 
                           RB, 
                           True
                           )
        
        return new_edge, chart.insert(new_edge, AdjoinTreeBuilder(chart, type(self).__name__, edge, adjoined_edge))

class AdjoinRuleRight(AbstractAdjoinRule):
    def application_filter(self, chart, grammar):
        return {
                "is_auxiliary": True,
                "treeposition": (),
                "dotposition": RA,
                "has_gap": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        for adjoined_edge in chart.select(
                                          adjoinable_symbol=edge.root_symbol(),
                                          dotposition=RB,
                                          span=edge.gap(),
                                          has_adjoined=False
                                          ):
            new_edge, inserted = self._insert_new(chart, edge, adjoined_edge)
            if inserted:
                yield new_edge

class AdjoinRuleLeft(AbstractAdjoinRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition":RB,
                "has_adjoined":False
                }
    
    def apply_iter(self, chart, grammar, adjoined_edge):
        for edge in chart.select(
                                 root_symbol=adjoined_edge.adjoinable_symbol(),
                                 gap=adjoined_edge.span(),
                                 is_auxiliary=True,
                                 treeposition=(),
                                 dotposition=RA,
                                 has_gap=True
                                 ):
            new_edge, inserted = self._insert_new(chart, edge, adjoined_edge)
            if inserted:
                yield new_edge
