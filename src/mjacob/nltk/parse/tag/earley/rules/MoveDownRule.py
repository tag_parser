# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.88]

    [γ,p,lb,i,j,k,l,0]
  → [γ,p+(0,),la,i,j,k,l,0]   γ[p+(0,)] is defined
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LB, LA
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder

class MoveDownRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LB,
                "has_children": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        new_edge = TagEdge(
                           edge.production(), 
                           edge.treeposition() + (0,), 
                           edge.span(), 
                           edge.gap(),
                           LA,
                           False
                           )
        if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
            yield new_edge