# This Python file uses the following encoding: utf-8
'''
Created on May 20, 2011

    [Kallmeyer 2010 p.86]
    
    [γ,p,la,i,j,k,l,0]
  → [γ,p,lb,l,−,−,l,0] fOA(γ, p) = 0
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LB, LA
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder

class PredictNoAdjRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "must_adjoin": False,
                "is_terminal": False
                }
    
    def apply_iter(self, chart, grammar, edge):
        new_edge = TagEdge(edge.production(), 
                           edge.treeposition(), 
                           (edge.end(), edge.end()), 
                           None, 
                           LB, 
                           False)
        if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
            yield new_edge
                