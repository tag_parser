# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.88]
    
    [γ,p·m,ra,i,j,k,l,0]
  → [γ,p,rb,i,j,k,l,0]   γ(p·m+1) is not defined

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, RA, RB
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder

class MoveUpRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": RA,
                "has_sibling": False,
                "is_root": False
                }
    
    def apply_iter(self, chart, grammar, edge):
        oldpos = edge.treeposition()
        newpos = tuple(oldpos[:-1])
        new_edge = TagEdge(edge.production(), 
                           newpos, 
                           edge.span(), 
                           edge.gap(),
                           RB,
                           False
                           )
        if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
            yield new_edge
                
