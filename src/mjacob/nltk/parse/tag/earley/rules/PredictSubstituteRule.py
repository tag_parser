# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.90]
    
    [γ,p,lb,i,–,–,i,0]
  → [α,ε,la,i,–,–,i,0] γ(p) a substitution node, α ∈ I,l(γ,p) = l(α,ε)    
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LB, LA
from mjacob.nltk.parse.tag.TreeBuilderI import InitialTreeBuilder

class PredictSubstituteRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LB,
                "length": 0,
                "has_gap": False,
                "is_substitution_site": True,
                }
    
    def apply_iter(self, chart, grammar, edge):
        for production in grammar.productions(edge.symbol(), is_initial=True):
            new_edge = TagEdge(production, (), (edge.end(), edge.end()), None, LA, False)
            if chart.insert(new_edge, InitialTreeBuilder(chart, type(self).__name__, new_edge)):
                yield new_edge
            