# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.87]
    
    [γ,p,rb,i,j,k,l,0]
    [β,pf,lb,i,−,−,i,0]    pf foot node address in β
  → [β,pf,rb,i,i,l,l,0]    β ∈ fSA(γ,p)
    
    ex: 
    [1:-:-:2] S (S c)[()] RB False
    [1:-:-:1] *S.N (S.N a *S.N)[(1,)] LB False
  → [1:1:2:2] *S.N (S.N a *S.N)[(1,)] RB False 

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, RB, LB
from nltk.tree import Tree
from mjacob.nltk.parse.tag.TreeBuilderI import TreeBuilderI

class CompleteFootTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, edge, foot_edge):
        if edge in working_on or foot_edge in working_on:
            return frozenset()
        
        oldpos = edge.treeposition()
        old_trees = [old_tree[oldpos] for old_tree in chart.get_trees(edge, working_on)]
        foot_trees = chart.get_trees(foot_edge, working_on)
        new_trees = []

        pos = foot_edge.treeposition()
        
        for foot_tree in foot_trees:
            tree = Tree.convert(foot_tree)
            
            for old_tree in old_trees:
                tree[pos] = old_tree
                new_trees.append(tree.freeze())
                
        return frozenset(new_trees)
    
class AbstractCompleteFootRule(AbstractSingleEdgeTagChartRule):
    def _insert_new(self, chart, edge, foot_edge):
        new_edge = TagEdge(
                           foot_edge.production(), 
                           foot_edge.treeposition(), 
                           edge.span(), 
                           edge.span(), 
                           RB, 
                           False
                           )

        return new_edge, chart.insert(new_edge, CompleteFootTreeBuilder(chart, type(self).__name__, edge, foot_edge))

class CompleteFootRuleRight(AbstractCompleteFootRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": RB,
                "has_adjoined": False,
                "can_adjoin": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        for foot_edge in chart.select(
                                      is_foot=True, 
                                      root_symbol=edge.symbol(),
                                      dotposition=LB,
                                      length=0,
                                      start=edge.start(),
                                      has_gap=False,
                                      ):
            
            new_edge, inserted = self._insert_new(chart, edge, foot_edge)
            if inserted:
                yield new_edge
                
class CompleteFootRuleLeft(AbstractCompleteFootRule):
    def application_filter(self, chart, grammar):
        return {
                "is_foot": True, 
                "dotposition": LB,
                "length": 0,
                "has_gap": False,
                }
    
    def apply_iter(self, chart, grammar, foot_edge):
        for edge in chart.select(
                                 symbol=foot_edge.root_symbol(),
                                 start=foot_edge.start(),
                                 dotposition=RB,
                                 has_adjoined=False,
                                 can_adjoin=True
                                 ):
            
            new_edge, inserted = self._insert_new(chart, edge, foot_edge)
            if inserted:
                yield new_edge
                
