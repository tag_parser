# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011

    [Kallmeyer 2010 p.85]
    
  → [α,ε,la,0,–,–,0,0], α ∈ I,l(α,ε) = S
    
@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractTagChartRule import AbstractTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LA
from mjacob.nltk.parse.tag.TreeBuilderI import InitialTreeBuilder

class InitializeRule(AbstractTagChartRule):
    NUM_EDGES = 0
    
    def apply_iter(self, chart, grammar):
        for production in grammar.productions(grammar.start(), is_initial=True):
            new_edge = TagEdge(production, (), (0,0), None, LA, False)
            if chart.insert(new_edge, InitialTreeBuilder(chart, type(self).__name__, new_edge)):
                yield new_edge