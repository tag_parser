# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

    [Kallmeyer 2010 p.86]
    
    [γ,p,la,i,j,k,l,0]
  → [γ,p,ra,i,j,k,l + 1,0]  l(γ, p) = wl+1 (the l+1-th token)
    

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractSingleEdgeTagChartRule import AbstractSingleEdgeTagChartRule
from mjacob.nltk.parse.tag.earley.TagEdge import TagEdge, LA, RA
from mjacob.nltk.parse.tag.TreeBuilderI import PassthroughTreeBuilder

class ScanTermRule(AbstractSingleEdgeTagChartRule):
    def application_filter(self, chart, grammar):
        return {
                "dotposition": LA,
                "is_terminal": True
                }
    
    def apply_iter(self, chart, grammar, edge):
        if edge.end() < chart.num_leaves() and edge.terminal() == chart.leaf(edge.end()):
            new_edge = TagEdge(
                               edge.production(), 
                               edge.treeposition(), 
                               (edge.start(), edge.end()+1), 
                               edge.gap(), 
                               RA, 
                               False
                               )
            if chart.insert(new_edge, PassthroughTreeBuilder(chart, type(self).__name__, edge)):
                yield new_edge
                
