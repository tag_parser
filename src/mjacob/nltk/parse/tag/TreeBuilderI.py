# This Python file uses the following encoding: utf-8
'''
Created on Jun 16, 2011

@author: mjacob
'''
from abc import abstractmethod

class TreeBuilderI(object, metaclass=ABCMeta):
    def __init__(self, chart, type, *edges):
        self.__chart = chart
        self.__type = type
        self.__edges = edges
    
    def build(self, working_on):
        return self._build(working_on, self.__chart, *self.__edges)
    
    @abstractmethod
    def _build(self, chart, *edges): pass
    
    def __eq__(self, othr):
        return self.__type == othr.__type and self.__edges == othr.__edges
    
    def __hash__(self):
        return hash((self.__type, self.__edges))
    
    def __str__(self):
        return "%s Tree Builder: %s" % (self.__type, self.__edges)

    def __repr__(self):
        return "%r Tree Builder: %r" % (self.__type, self.__edges)

class InitialTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, new_edge):
        return frozenset((new_edge.production().as_tree(),))
    
class PassthroughTreeBuilder(TreeBuilderI):
    def _build(self, working_on, chart, edge):
        if edge in working_on:
            return frozenset()
        
        return chart.get_trees(edge, working_on)