# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011

This is a hacked version of what's in NLTK. 

it's available under the Apache 2.0 license:
http://www.apache.org/licenses/LICENSE-2.0

# Copyright (C) 2001-2011 NLTK Project
# Author: Edward Loper <edloper@gradient.cis.upenn.edu>
#         Steven Bird <sb@csse.unimelb.edu.au>
#         Jean Mark Gawron <gawron@mail.sdsu.edu>
#         Peter Ljunglöf <peter.ljunglof@heatherleaf.se>
# URL: <http://www.nltk.org/>
# For license information, see LICENSE.TXT

@author: mjacob
'''
from nltk.tree import Tree

class TagChart(object):
    """
    A blackboard for hypotheses about the syntactic constituents of a
    sentence.  A chart contains a set of edges, and each edge encodes
    a single hypothesis about the structure of some portion of the
    sentence.

    The L{select} method can be used to select a specific collection
    of edges.  For example C{chart.select(is_complete=True, start=0)}
    yields all complete edges whose start indices are 0.  To ensure
    the efficiency of these selection operations, C{Chart} dynamically
    creates and maintains an index for each set of attributes that
    have been selected on.

    In order to reconstruct the trees that are represented by an edge,
    the chart associates each edge with a set of child pointer lists.
    A X{child pointer list} is a list of the edges that license an
    edge's right-hand side.

    @ivar _tokens: The sentence that the chart covers.
    @ivar _num_leaves: The number of tokens.
    @ivar _edges: A list of the edges in the chart
    @ivar _indexes: A dictionary mapping tuples of edge attributes
        to indices, where each index maps the corresponding edge
        attribute values to lists of edges.
    """
    def __init__(self, tokens, edge_type):
        """
        Construct a new chart. The chart is initialized with the 
        leaf edges corresponding to the terminal leaves.

        @type tokens: L{list}
        @param tokens: The sentence that this chart will be used to parse.
        """
        # Record the sentence token and the sentence length.
        self.__tokens = tuple(tokens)
        self.__num_leaves = len(self.__tokens)
        self.__leaf_indices = dict((leaf, tuple(i 
                                                for i in range(len(tokens)) 
                                                if tokens[i] == leaf)) 
                                   for leaf in frozenset(tokens))
        
        self.__edge_type=edge_type
        
        # Initialise the chart.
        self.initialize()

    def initialize(self):
        """
        Clear the chart.
        """
        # A list of edges contained in this chart.
        self.__edges = []
        
        # The set of child pointer lists associated with each edge.
        self.__edge_treebuilders = {}
        self.__cached_trees = {}

        # Indexes mapping attribute values to lists of edges 
        # (used by select()).
        self.__indexes = {}
    
    def __str__(self):
        return "\n".join(map(str, [type(self),
                                   "  tokens: %s" % (self.__tokens,), 
                                   "  leaves: %s" % (self.__num_leaves,), 
                                   "  indices: %s" % ("\n           ".join(str(y) for y in list(self.__leaf_indices.items()))), 
                                   "  edges: %s" % ("\n         ".join(str(y) for y in self.__edges)), 
                                   "  indexes: %s" % ("\n           ".join(str(y) for y in list(self.__indexes.items()))),
                                   ]))
    
    #////////////////////////////////////////////////////////////
    # Sentence Access
    #////////////////////////////////////////////////////////////

    def num_leaves(self):
        """
        @return: The number of words in this chart's sentence.
        @rtype: C{int}
        """
        return self.__num_leaves

    def leaf(self, index):
        """
        @return: The leaf value of the word at the given index.
        @rtype: C{string}
        """
        return self.__tokens[index]

    def leaves(self):
        """
        @return: A list of the leaf values of each word in the
            chart's sentence.
        @rtype: C{list} of C{string}
        """
        return self.__tokens
    
    def leaf_indices(self, leaf):
        """
        @return: A list of indices at which @param leaf appears in the sentence
        @rtype: C{tuple} of C{int}
        """
        return self.__leaf_indices.get(leaf, ())

    #////////////////////////////////////////////////////////////
    # Edge access
    #////////////////////////////////////////////////////////////

    def edges(self):
        """
        @return: A list of all edges in this chart.  New edges
            that are added to the chart after the call to edges()
            will be contained in this list, but you shouldn't rely on that.
        @rtype: C{list} of L{EdgeI}
        @see: L{iteredges}, L{select}
        """
        return self.__edges

    def iteredges(self):
        """
        @return: An iterator over the edges in this chart.  It is 
            I{not} guaranteed that new edges which are added to the 
            chart before the iterator is exhausted will also be  
            generated.
        @rtype: C{iter} of L{EdgeI}
        @see: L{edges}, L{select}
        """
        return iter(self.__edges)

    # Iterating over the chart yields its edges.
    __iter__ = iteredges

    def num_edges(self):
        """
        @return: The number of edges contained in this chart.
        @rtype: C{int}
        """
        return len(self.__edges)

    def select(self, **restrictions):
        """
        @return: An iterator over the edges in this chart.  Any
            new edges that are added to the chart before the iterator
            is exahusted will also be generated.  C{restrictions}
            can be used to restrict the set of edges that will be
            generated.
        @rtype: C{iter} of L{EdgeI}
        """
        # If there are no restrictions, then return all edges.
        if restrictions=={}: return iter(self.__edges)
            
        # Find the index corresponding to the given restrictions.
        restr_keys = list(restrictions.keys())
        restr_keys.sort()
        restr_keys = tuple(restr_keys)

        # If it doesn't exist, then create it.
        if restr_keys not in self.__indexes:
            self._add_index(restr_keys)
                
        vals = tuple(restrictions[key] for key in restr_keys)
        return iter(self.__indexes[restr_keys].get(vals, []))
    
    def _add_index(self, restr_keys):
        """
        A helper function for L{select}, which creates a new index for
        a given set of attributes (aka restriction keys).
        """
        # Make sure it's a valid index.
        for key in restr_keys:
            if not hasattr(self.__edge_type, key):
                raise ValueError('Bad restriction: %s' % key)

        # Create the index.
        index = self.__indexes[restr_keys] = {}

        # Add all existing edges to the index.
        for edge in self.__edges:
            vals = tuple(getattr(edge, key)() for key in restr_keys)
            index.setdefault(vals, []).append(edge)
    
    def _register_with_indexes(self, edge):
        """
        A helper function for L{insert}, which registers the new
        edge with all existing indexes.
        """
        for (restr_keys, index) in list(self.__indexes.items()):
            vals = tuple(getattr(edge, key)() for key in restr_keys)
            index.setdefault(vals, []).append(edge)
    
    #////////////////////////////////////////////////////////////
    # Edge Insertion
    #////////////////////////////////////////////////////////////

    def insert(self, edge, treebuilder):
        """
        Add a new edge to the chart.

        @type edge: L{EdgeI}
        @param edge: The new edge
        @rtype: C{bool}
        @return: True if this operation modified the chart.  In
            particular, return true iff the chart did not already
            contain C{edge}
        """
        
        # Is it a new edge?
        if edge not in self.__edge_treebuilders:
            #print "    new edge: %s" % (edge)
            # Add it to the list of edges.
            self._append_edge(edge)
            # Register with indexes.
            self._register_with_indexes(edge)
            self.__edge_treebuilders[edge] = set((treebuilder,))
            
            return True
        else:
            #print "    duplicate edge: %s" % (edge)
            self.__edge_treebuilders[edge].add(treebuilder)
            return False
    
    def get_trees(self, edge, working_on=set()):
        """
        find the trees which can be built out of the specified edge.
        
        the parameter "working_on" should not be specified by the user, as it is
        an internal mechanism that is used to avoid loops in the recursion.
        """
        if edge in self.__cached_trees:
            return self.__cached_trees[edge]
        
        else:
            working_on.add(edge)
            treebuilders = self.__edge_treebuilders[edge]
            if len(treebuilders) == 1:
                trees = None
                for treebuilder in treebuilders:
                    trees = treebuilder.build(working_on)
            else:
                trees = []
                for treebuilder in treebuilders:
                    trees.extend(treebuilder.build(working_on))
                trees = frozenset(trees)
            self.__cached_trees[edge] = trees
            working_on.remove(edge)
            return trees
    
    def _append_edge(self, edge):
        self.__edges.append(edge)
    
    #////////////////////////////////////////////////////////////
    # Tree extraction & child pointer lists
    #////////////////////////////////////////////////////////////

    def accept(self, root, strategy):
        """returns True if there's a goal item in the chart"""
        return strategy.goal_found(self, root)

    """parses(self.__grammar.start(), self.__strategy, tree_class=tree_class)"""
    def parses(self, root, strategy, tree_class=Tree):
        trees = set()
        for goal_edge in strategy.goal_edges(self, root):
            trees.update(self.get_trees(goal_edge))
        return list(trees)
