# This Python file uses the following encoding: utf-8
'''
Created on May 20, 2011

@author: mjacob
'''
from mjacob.nltk.parse.tag.AbstractTagChartRule import AbstractTagChartRule

class AbstractSingleEdgeTagChartRule(AbstractTagChartRule):
    """
    Represents a TagChartRule w/ a single edge
    """
    
    NUM_EDGES = 1
    
    def apply_everywhere_iter(self, chart, grammar):
        for edge in chart.select(**self.application_filter(chart, grammar)):
            for new_edge in self.apply_iter(chart, grammar, edge):
                yield new_edge