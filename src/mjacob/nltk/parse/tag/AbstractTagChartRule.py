# This Python file uses the following encoding: utf-8
'''
Created on May 18, 2011


@author: mjacob
'''
from itertools import product
from nltk.parse.chart import AbstractChartRule

class AbstractTagChartRule(AbstractChartRule):
    def application_filter(self, chart, grammar):
        return {}
    
    def applies_to(self, chart, grammar, edge):
        """
        i was going to do some sophisticated caching of this data, but
        now i'm pretty sure it's only called once for every edge/rule combo
        so that would be a complete waste
        """
        for key, value in list(self.application_filter(chart, grammar).items()):
            if not hasattr(edge, key):
                raise ValueError("unexpected key %s" % (key))
            if getattr(edge, key)() != value:
                return False
        return True
    
    # Default: loop through the given number of edges, and call
    # self.apply() for each set of edges.
    def apply_everywhere_iter(self, chart, grammar):
        for edges in product(chart, repeat=self.NUM_EDGES):
            for new_edge in self.apply_iter(chart, grammar, *edges):
                yield new_edge
    
    def apply(self, *args):
        super(AbstractTagChartRule, self).apply(*args)
