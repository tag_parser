# This Python file uses the following encoding: utf-8
'''
Created on Apr 27, 2011

Code which iterates possible paths for logical deductions.
For chart parsing, this is potentially useful for discovering ambiguities in a tree,
but as the Earley Tag rules are far from optimal, much more ambiguity is discovered
than actually results in different parses. 

More generally, this code is interesting as I haven't seen anything like it before.
There's potentially some interesting combinatorics going on here - we're counting 
the number of DAGs as sub-graphs of a directed hypergraph. 

@author: mjacob
'''
from functools import reduce
def deductions(result, 
               get_anteceding_rules, 
               get_antecedents=lambda x: x, 
               working_on=set(), 
               resolved=set()):
    """
    given a collections of lemmas 
      A1, A2, A3 
    
    and logical deductions of the type
      A1,A2 -> A3   (read: A1 and A2 imply A3)
    
    iterate over all possible relevant deductive pathways which result in C{result}
    
    arguments:
        result: an object which represents the final lemma of a deduction
        get_anteceding_rules: a callable item (e.g., a function) of the form
                f(lemma) -> deductions
            where each deduction directly generates the supplied lemma
        get_antecedents: a callable item (e.g., a function) of the form
                f(rule) -> lemmas
            where the resulting lemmas are the antecedents of the rule

    result:
        a generator which iterates deductions of the form
              ((A, (B, C)), (B, ()), (C, ()))
          where each element represents a deduction rule, consisting of a resulting lemma and its antecedents.
    """
    
    def mark_resolved():
        working_on.remove(result)
        resolved.add(result)
    
    def mark_unresolved():
        resolved.remove(result)
        working_on.add(result)
        
    def begin_resolution():
        working_on.add(result)
        
    def finish_resolution():
        working_on.remove(result)
    
    def is_recursive_rule(rule):
        for lemma in get_antecedents(rule):
            if lemma in working_on:
                return True
        return False
    
    def resolve_antecedents(unresolved_antecedents):
        """iterate over all possible combinations of further productions (v)
        this method was created because python's itertools.product method had side effects."""
        first, rest = unresolved_antecedents[0], unresolved_antecedents[1:]
        
        for deduction_of_first in deductions(first, 
                                             get_anteceding_rules, 
                                             get_antecedents, 
                                             working_on, 
                                             resolved):
            if rest:
                for deduction_of_rest in resolve_antecedents(rest):
                    yield (deduction_of_first,) + deduction_of_rest
            
            else:
                yield (deduction_of_first,)

    if result in resolved:
        """if there is a merging dependency"""
        yield ()
    
    else:
        begin_resolution()
        
        for rule in get_anteceding_rules(result):
            
            if is_recursive_rule(rule):
                """ignore looping deductions""" 
                continue
            
            unresolved_antecedents = tuple(lemma 
                                           for lemma in get_antecedents(rule) 
                                           if lemma not in resolved)
            
            if not unresolved_antecedents:
                mark_resolved()
                yield ((result, rule),)
                mark_unresolved() 
            
            else:
                for deductions_of_antecedents in resolve_antecedents(unresolved_antecedents):
                    mark_resolved()
                    yield ((result, rule),) + reduce(tuple.__add__,
                                                     deductions_of_antecedents)
                    mark_unresolved()
                        
        finish_resolution()

    
if __name__ == "__main__":
    """test functionality"""
    #start, A = 1, {1: ((2,),(2,3,),(3,),), 2: ((3,),(4,),), 3: ((4,),(5,),(6,),), 4: ((),), 5: ((6,),), 6: ((),)}
    start, A = 1, {1: ((2,3,),), 2: ((3,), (4,),), 3: ((2,), (4,), (4,5,),), 4: ((),), 5: ((),)}
    #start, A = 1, {1: ((2,3,),), 2: ((3,), (4,),), 3: ((2,), (2,4,), (4,),), 4: ((),)}
    #start, A = 12, {0: ((),), 1: ((2,),), 2: ((10, 11),), 3: ((),), 4: ((5, 6),), 5: ((8, 9),), 6: ((2, 18),), 7: ((5, 1),), 8: ((),), 9: ((),), 10: ((),), 11: ((),), 12: ((13, 6),), 13: ((3,),), 14: ((13,),), 15: ((5,),), 16: ((),), 17: ((13, 1),), 18: ((37,),), 19: ((),), 20: ((21,),), 21: ((),), 22: ((20,),), 23: ((24,),), 24: ((),), 25: ((23,),), 26: ((),), 27: ((),), 28: ((),), 29: ((30, 31),), 30: ((),), 31: ((),), 32: ((29,),), 33: ((30, 34),), 34: ((),), 35: ((10, 16),), 36: ((35,),), 37: ((30, 0),), 38: ((35, 32),), 39: ((20, 32),), 40: ((33,),)}
    for deduction in deductions(start, A.__getitem__):
        print("%s," % (deduction,))
        #print ", ".join("%s -> %s" % (antecedents, consequent) for consequent, antecedents in reversed(deduction))
    