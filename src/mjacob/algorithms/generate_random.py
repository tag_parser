# This Python file uses the following encoding: utf-8
'''
Created on May 14, 2011

helper methods for generating random sentences from a context free grammar

this is used both for performance testing and for generating content which
can be used to check the accuracy of parses against existing models.

@author: mjacob
'''
import random
from nltk.grammar import Nonterminal
from nltk.parse.earleychart import EarleyChartParser

def generate_random_sentence(cfg_grammar, maxdepth=8):
    """
    given a ContextFreeGrammar cfg_grammar, and an optional maximum depth (maxdepth, default = 8)
    construct a string which is acceptable to the grammar.
    
    the maxdepth option specifies the maximum number of recursive applications of the grammar
    used to generate a string. 
    
    warning: if maxdepth is too small, in its current implementation, this method may never terminate.  
    """
    start_rule = random.choice(tuple(cfg_grammar.productions(lhs=cfg_grammar.start())))
    while True:
        sent = " ".join(random_rule(start_rule, cfg_grammar, maxdepth))
        if "  " in sent or sent.endswith(" "): continue
        return sent
    
def random_rule(rule, grammar, maxdepth=8):
    """
    randomly apply the given rule according to the given grammar.
    
    if maxdepth is 0, stop the iteration.  
    """
    if not maxdepth:
        raise StopIteration
    for rhs in rule.rhs():
        if type(rhs) is Nonterminal:
            start_rule = random.choice(tuple(grammar.productions(lhs=rhs)))
            yield " ".join(random_rule(start_rule, grammar, maxdepth-1))
        else:
            yield rhs

if __name__ == "__main__":
    import nltk
    g = nltk.data.load('grammars/spanish_grammars/spanish2.cfg')
    p = EarleyChartParser(g)
    s = "un mesa con el amiga sobre los hombre vio una hombre sobre las mesa con una mesa con las noticia sobre el hombre"
    parse = p.nbest_parse(s.split(' '))
    print(len(set([str(tree) for tree in parse])))
    
    exit(0)
    for i in range(100):
        sent = generate_random_sentence(g)
        tokens = sent.split(' ')
        #if len(tokens) > 15:
        #    continue
        print(sent)
        print(len(p.nbest_parse(tokens)))