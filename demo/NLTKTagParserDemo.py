# This Python file uses the following encoding: utf-8
'''
Created on Jun 14, 2011

@author: mjacob
'''
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar
from mjacob.nltk.parse.tag.TagChartParser import TagChartParser
from itertools import chain
import yaml


l4_grammar = TreeAdjoiningGrammar(filename='../tests/integration/tag_chart/L4-trinary/grammar.yaml')
l4_parser = TagChartParser(l4_grammar)

print("Parsing non-context-free languages")
print()
print("EXAMPLE 1: L4 {aⁿbⁿcⁿdⁿ : n∈ℕ}")
print("see https://secure.wikimedia.org/wikipedia/en/wiki/Context-sensitive_grammar")
print("  %s" % (l4_grammar))
for production in chain(sorted(l4_grammar.productions(is_auxiliary=False)), sorted(l4_grammar.productions(is_auxiliary=True))):
    print("   %s" % (production))
print()
for n in 3,4:
    string = "a"*n + "b"*n + "c"*n + "d"*n
    tokens = tuple(c for c in string)
    print("  parsing '%s': " % (string,))
    for parse in l4_parser.nbest_parse(tokens):
        print("    %s" % (parse.pprint(margin=100000)))

print()
print()

xs_grammar = TreeAdjoiningGrammar(filename='../tests/integration/tag_chart/cross_serial_2/grammar.yaml')
xs_parser = TagChartParser(xs_grammar)

print("EXAMPLE 2: Cross serial dependencies")
print("see http://www.let.rug.nl/~vannoord/papers/acl94/node5.html")
print("  %s" % (xs_grammar))
for production in chain(sorted(xs_grammar.productions(is_auxiliary=False)), sorted(xs_grammar.productions(is_auxiliary=True))):
    print("   %s" % (production))
print()
examples = yaml.load(open('../tests/integration/tag_chart/cross_serial_2/tests.yaml').read())
for string in examples:
    tokens = string.split(' ')
    print("  parsing '%s': " % (string,))
    for parse in xs_parser.nbest_parse(tokens):
        print("    %s" % (parse.pprint(margin=100000)))

print()
print()

elephant_grammar = TreeAdjoiningGrammar(filename='../tests/integration/tag_chart/elephant/grammar.yaml')
elephant_parser = TagChartParser(elephant_grammar)
print("Example 3: An ambiguous sentence")
print("  %s" % (elephant_grammar))
for production in chain(sorted(elephant_grammar.productions(is_auxiliary=False)), sorted(elephant_grammar.productions(is_auxiliary=True))):
    print("   %s" % (production))
print()

examples = yaml.load(open('../tests/integration/tag_chart/elephant/tests.yaml').read())
for string in examples:
    tokens = string.split(' ')
    print("  parsing '%s': " % (string,))
    for parse in elephant_parser.nbest_parse(tokens):
        print("    %s" % (parse.pprint(margin=100000)))

print()
print()

print("Performance testing results")
with open('../tests/performance/PERFORMANCE_RESULTS.txt') as file:
    print(file.read())