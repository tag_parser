# This Python file uses the following encoding: utf-8
'''
Created on May 14, 2011

@author: mjacob
'''
# This Python file uses the following encoding: utf-8
# -*- coding: UTF-8 -*-
from nltk.parse.earleychart import EarleyChartParser
import nltk
from nltk.tree import ImmutableTree
from mjacob.algorithms.generate_random import generate_random_sentence
""" integration tests - does a specific parser puke on any of these TAGs? """
import argparse
import sys
import re
import logging
logging.basicConfig(level=logging.WARN, stream=sys.stdout)

def get_class(module_name, class_name):
    return __import__(module_name, fromlist=[class_name]).__getattribute__(class_name)

class ParseTests(object):
    """this class will generate random sentences from a using a context free grammar,
    and then test that the results of my TAG parsers are identical to the results of
    a standard NLTK parser."""

    def __init__(self, args):
        self.args=args

    def run_tests(self, n=100, grammar_file='grammars/spanish_grammars/spanish2.cfg'):
        parser_module_name, parser_class_name = re.match('(.*)\.(\w+)', self.args.parser).groups()
        parser_class = get_class(parser_module_name, parser_class_name)

        grammar = nltk.data.load(grammar_file)
        parser = parser_class(grammar)
        
        base_parser = EarleyChartParser(grammar)
        
        errors = []
        err = False
        for i in range(n):
            print(i, end=' ')
            sent = generate_random_sentence(grammar)
            print(len(errors), end=' ')
            print(sent, end=' ')
            tokens = sent.split(' ')
            good_parses = set(base_parser.nbest_parse(tokens, tree_class=ImmutableTree))
            print(len(good_parses), end=' ')
            if len(good_parses) > 5:
                print("skipping!")
                continue
            found_parses = set(parser.nbest_parse(tokens))
            print(len(found_parses))
            
            if found_parses != good_parses:
                if len(found_parses) != len(good_parses):
                    errors.append("different number of parses found for \"%s\" (%s vs %s)" % (sent, len(good_parses), len(found_parses)))
                else:
                    errors.append("different parses found for \"%s\" (%s)" % (sent, len(good_parses)))
                if not err and len(good_parses) == 1:
                    print("GOOD:")
                    print("\n".join(repr(x) for x in good_parses))
                    print("FOUND:")
                    print("\n".join(repr(x) for x in found_parses))
                    
                    err = True
            
        if errors:
            print("%s errors (out of %s tests)" % (len(errors), n))
            print("\n".join(errors))
            return 1
        
        else:
            print("%s tests passed" % (n))

def parse_arguments():
    parser = argparse.ArgumentParser(description='integration tests for CFG parsers')
    parser.add_argument('parser', metavar='parser class', type=str, 
                        help='a cfg parser class')
    return parser.parse_args()

def main():
    args = parse_arguments()
    test_runner = ParseTests(args)
    exit_code = test_runner.run_tests()
    exit(exit_code)

if __name__ == "__main__":
    main()
