# This Python file uses the following encoding: utf-8
# -*- coding: UTF-8 -*-
""" integration tests - does a specific parser puke on any of these TAGs? """
import argparse
import yaml
import os
import sys
from nltk.tree import Tree
import re
import logging
from nltk.grammar import Nonterminal
logging.basicConfig(level=logging.WARN, stream=sys.stdout)

def get_class(module_name, class_name):
    return __import__(module_name, fromlist=[class_name]).__getattribute__(class_name)

BASEDIR = os.path.split(os.path.abspath(__file__))[0] 

class ParseTests(object):
    """this class is used to run full tests of the TAG parsers. 
    
    given a grammar, some sentences, and their expected parses, is the
    output of a specified TAG parser correct?
    """
    
    def __init__(self, args):
        self.__args=args

    def _all_tests(self):
        """assume the test dirs are all the dirs inside the directory
           containing this script """
        return [x for x in os.listdir(BASEDIR) if os.path.isdir(os.path.join(BASEDIR, x))]

    def read_tests(self, test_path):
        return list(yaml.load(open(os.path.join(test_path,
                                           "tests.yaml")).read()).items())

    def run_tests(self):
        if self.__args.tests:
            tests = self.__args.tests.split(',')
        else:
            tests = self._all_tests()

        grammar_module_name, grammar_class_name = re.match('(.*)\.(\w+)', self.__args.grammar).groups()
        grammar_class = get_class(grammar_module_name, grammar_class_name)
        parser_module_name, parser_class_name = re.match('(.*)\.(\w+)', self.__args.parser).groups()
        parser_class = get_class(parser_module_name, parser_class_name)
        strategy_module_name, strategy_name = re.match('(.*)\.(\w+)', self.__args.strategy).groups()
        strategy_obj = get_class(strategy_module_name, strategy_name)

        print("testing w/ parser %s, strategy %s" % (parser_class, strategy_name))
        print("%s tests" % (len(tests)))

        total_passed = 0
        total_failed = 0
        for test in tests:
            test_path = os.path.join(sys.path[0], test)
            grammar = grammar_class(os.path.join(test_path, "grammar.yaml"))
            parser = parser_class(grammar, strategy=strategy_obj)
            
            print("starting to test grammar '%s':" % (test))
            failures = []
            passes = 0
            for sentence, trueparse in self.read_tests(test_path):
                if not trueparse:
                    raise Exception("there's something wrong w/ your parse, hombre (%s)" % (sentence))
                
                if type(trueparse) is list: # multiple possible
                    passed = self._parse_all(parser, sentence, trueparse, failures)
                else:
                    passed = self._parse_one(parser, sentence, trueparse, failures)
                    #passed = self._parse_all(parser, sentence, [trueparse], failures)

                if passed:
                    passes += 1
            
            if failures:
                print("  %s tests passed, %s failed" % (passes, len(failures)))
                for failure in failures:
                    print("  " + failure)
            else:
                print("  %s tests passed" % (passes))
            total_passed += passes
            total_failed += len(failures)
        if total_failed:
            print("TOTAL: %s tests passed, %s failed" % (total_passed, total_failed))
            return 1
        else:
            print("TOTAL: %s tests passed" % (total_passed))
            return 0

    def _parse_all(self, parser, sentence, trueparses, failures):
        if sentence == '':
            tokens = []
        else:
            tokens = sentence.split(' ')

        accepted = parser.accept(tokens)
        trueparses = set((self._convert_true_parse(trueparse) for trueparse in trueparses))
        
        if not accepted:
            failures.append('parser did not accept \'%s\'' % (sentence))
            return False
        
        elif not self.__args.accept_only:
            try:
                parses = parser.nbest_parse(tokens)
            except:
                print(sentence)
                raise
            if parses is None or len(parses) == 0:
                failures.append('parsing failed: \'%s\'' % (sentence))
                return False
            elif set(parses) != trueparses:
                failures.append("parses not equal (expected '%s', got '%s')" % ([tree.pprint(margin=10000) for tree in trueparses], [tree.pprint(margin=10000) for tree in parses]))
                return False
            else:
                return True
        
        return True
    
    def _convert_true_parse(self, trueparse):
        tree = Tree(trueparse)
        for pos in tree.treepositions():
            subtree = tree[pos]
            if isinstance(subtree, Tree):
                subtree.node = Nonterminal(subtree.node)
        tree= tree.freeze()
        return tree

    def _parse_one(self, parser, sentence, trueparse, failures):
        if sentence == '':
            tokens = []
        else:
            tokens = sentence.split(' ')
            
        accepted = parser.accept(tokens)
        trueparse = self._convert_true_parse(trueparse)
        if not accepted:
            failures.append('parser did not accept \'%s\'' % (sentence))
            return False
        
        elif not self.__args.accept_only:
            try:
                parse = parser.parse(tokens)
            except:
                print(sentence)
                raise
            if parse is None:
                failures.append('parsing failed: \'%s\'' % (sentence))
                return False
            elif parse != trueparse:
                failures.append("parses not equal (expected '%s', got '%s')" % (trueparse.pprint(margin=10000), parse.pprint(margin=10000)))
                return False
            else:
                return True
        
        return True

def parse_arguments():
    parser = argparse.ArgumentParser(description='integration tests for TAG parsers')
    parser.add_argument('parser', metavar='parser class', type=str, 
                        help='a tag parser class')
    parser.add_argument('-g', '--grammar', dest='grammar', 
                        default='mjacob.nltk.grammar.TreeAdjoiningGrammar.TreeAdjoiningGrammar',
                        help='a tag grammar class (must be able to read yaml)')
    parser.add_argument('-s', '--strategy', dest='strategy',
                        default='mjacob.nltk.parse.tag.earley.rules.TAG_EARLEY_STRATEGY',
                        help='the strategy used to parse')
    parser.add_argument('-t', '--tests', dest='tests',
                        help='only test the specified grammars')
    parser.add_argument('--accept-only', dest='accept_only', default=False, action="store_true")
    return parser.parse_args()

def main():
    args = parse_arguments()
    test_runner = ParseTests(args)
    exit_code = test_runner.run_tests()
    exit(exit_code)

if __name__ == "__main__":
    main()
