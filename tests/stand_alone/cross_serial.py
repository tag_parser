# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

@author: mjacob
'''
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar
from mjacob.nltk.parse.tag.TagChartParser import TagChartParser
from mjacob.nltk.parse.tag.earley.rules import TAG_EARLEY_STRATEGY

grammar = TreeAdjoiningGrammar(filename="../integration/tag_chart/cross_serial_2/grammar.yaml")
parser = TagChartParser(grammar)

for s in ["Jan Piet Marie de kinderen zag helpen leren zwemmen"]:
    tokens = s.split(' ')
    chart = parser.chart_parse(tokens)
#    for goal_edge, deduction in chart.deduction_paths(grammar.start().symbol(), TAG_EARLEY_STRATEGY):
#        print "GOAL: %s" % (goal_edge)
#        i = 1
#        for x,y,z in deduction:
#            print "% 6i %s <- %s" % (i, x, y)
#            for tree in z:
#                print "       %s" % (z,)
#            i += 1
    
    print("   %s" % ("\n   ".join(tree.pprint(margin=10000) for tree in chart.parses(grammar.start().symbol(), TAG_EARLEY_STRATEGY))))
    #print parser.nbest_parse(tokens)
    #print parser.parse(tokens)