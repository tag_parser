# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

@author: mjacob
'''
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar
from mjacob.nltk.parse.tag.TagChartParser import TagChartParser
from mjacob.nltk.parse.tag.prefix_valid_earley.rules import TAG_PREFIX_VALID_EARLEY_STRATEGY

grammar = TreeAdjoiningGrammar(filename="../integration/tag_chart/squares/grammar.yaml")
parser = TagChartParser(grammar, strategy=TAG_PREFIX_VALID_EARLEY_STRATEGY)

for s in ["a a a a"]:
    tokens = s.split(' ')
    chart = parser.chart_parse(tokens)
    print("   %s" % ("\n   ".join(str(x) for x in chart.parses(grammar.start().symbol(), TAG_PREFIX_VALID_EARLEY_STRATEGY))))
    #print parser.nbest_parse(tokens)
    #print parser.parse(tokens)