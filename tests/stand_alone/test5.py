# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

@author: mjacob
'''
import nltk
from nltk.parse.chart import ChartParser
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar
from mjacob.nltk.parse.tag.TagChart import TagChart

grammar = TreeAdjoiningGrammar(nltk.data.load('grammars/sample_grammars/toy.cfg'))
parser = ChartParser(grammar, 
                     strategy=[],
                     chart_class=TagChart)