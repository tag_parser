# This Python file uses the following encoding: utf-8
'''
Created on May 30, 2011

@author: mjacob
'''
import nltk
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar
from mjacob.nltk.parse.tag.earley.rules import TAG_EARLEY_STRATEGY
from mjacob.nltk.parse.tag.TagChartParser import TagChartParser

VAR = 1

raw_data = nltk.data.load('grammars/large_grammars/atis_sentences.txt', format='raw')
sentences = [x[0] for x in nltk.parse.util.extract_test_sentences(raw_data)][:1]

if VAR == 0:
    grammar = TreeAdjoiningGrammar(cfg=nltk.data.load('grammars/large_grammars/atis.cfg'))
elif VAR == 1:
    grammar = TreeAdjoiningGrammar("../integration/tag_chart/degenerate_1/grammar.yaml")
elif VAR == 2:
    grammar = TreeAdjoiningGrammar("../integration/tag_chart/cross_serial_2/grammar.yaml")

parser = TagChartParser(grammar)

if VAR == 0:
    chart = parser.chart_parse(sentences[0])
elif VAR == 1:
    chart = parser.chart_parse(['John'])
elif VAR == 2:
    chart = parser.chart_parse("Jan Piet Marie de kinderen zag helpen leren zwemmen".split(' '))

print(chart)
print("\n\n\n")
print(chart.parses(grammar.start().symbol(), TAG_EARLEY_STRATEGY))