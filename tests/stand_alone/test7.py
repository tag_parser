# This Python file uses the following encoding: utf-8
'''
Created on Jun 2, 2011

@author: mjacob
'''
from mjacob.collections.FrozenIndex import FrozenIndex


x = FrozenIndex(((1,1),(1,2),(2,3),(2,4)))

for y in x:
    print(y)