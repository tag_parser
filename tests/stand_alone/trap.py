# This Python file uses the following encoding: utf-8
'''
Created on May 19, 2011

@author: mjacob
'''
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar
from mjacob.nltk.parse.tag.TagChartParser import TagChartParser
from mjacob.nltk.parse.tag.earley.rules import TAG_EARLEY_STRATEGY

grammar = TreeAdjoiningGrammar(grammar="""
start: S
productions:
    - (S S S)   
    - (S S)
    - (S 'b')                  
                     """)
parser = TagChartParser(grammar)

for s in ["b"]:
    tokens = s.split(' ')
    chart = parser.chart_parse(tokens)
    print(chart.num_parses(grammar.start().symbol(), TAG_EARLEY_STRATEGY))
