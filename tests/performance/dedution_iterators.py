# This Python file uses the following encoding: utf-8
'''
Created on May 14, 2011

This class was to test the functionality and performance of a deduction
iterator which accumulated partial results through tuple addition,
against one returned it as a growing deque. their performance was 
nearly identical.  

@author: mjacob
'''
import timeit

from itertools import chain, combinations
from mjacob.algorithms.deduction_iterator import deductions

# 1 2 8 98 3728
go = "for ded in deductions(start, A.__getitem__):pass"
for n in range(1,8):
    m = 0#12241244
    
    A = dict((i, tuple(g 
                       for g in chain(*[combinations(range(n), m) 
                                        for m in range(n+1)]) # xrange(4) 
                       if i not in g))
             for i in range(n))
    
    #A = dict((i, tuple((j,k) for j,k in product(xrange(n+1), xrange(n+1,n+n+2)) if j is not i and k is not i)) for i in xrange(n+n+1) if i != n)
    #print "\n".join(str(x) for x in sorted(A.items()))
    #A[n] = ((),)
    #A[n+n+1] = ((),)
    x="CBA"
    for d in deductions(0, A.__getitem__):
        if len(d) < n: continue
        #print u"; ".join(u"%s -> %s" % (",".join(str(x[j]) for j in b), x[a]) for a,b in d)
        m += 1

    print("%s %s" % (n, m))    
    for d in ("deduction_iterator", "deduction_iterator3"):
        continue
        prep = """
from algorithms.%s import deductions
from itertools import product
start=0
n=%s
A = %s
""" % (d, n, A)
        timer = timeit.Timer(go, prep)
        
        print("%s %s (%s): %.2f ms/pass" % (d, n, m, 1000 * timer.timeit(number=1)))
