# This Python file uses the following encoding: utf-8
'''
Created on May 17, 2011

compare the performance of various parsers using the ATIS grammar.

(extracted from a treebank of the DARPA ATIS3
http://www.informatics.sussex.ac.uk/research/groups/nlp/carroll/cfg-resources/
context free, *NOT* CNF (nodes w/ 10 children), 5517 productions, 549 non-terminals, 925 terminals
average sentence length: 11.408163265306122

Note that there are slight differences in the way that my parsers and the NLTK
parsers are invoked. Due to a design oversight, my parsers cannot feasibly
compute the parse trees of complex senteces with large grammars, so instead 
I merely ask whether they recognize a string as valid. As the NLTK parsers
do not have a similar method, I cannot compare them directly.

In any event, my parsers cannot even recognize sentences from this grammar
in a reasonable length of time. 

@author: mjacob
'''
import nltk
from ParsePerformanceTester import ParsePerformanceTester, NLTK_BEST, MY_PARSER

grammar = nltk.data.load('grammars/large_grammars/atis.cfg')
grammar_string = "nltk.data.load('grammars/large_grammars/atis.cfg')"

raw_data = nltk.data.load('grammars/large_grammars/atis_sentences.txt', format='raw')
sentences = [x[0] for x in nltk.parse.util.extract_test_sentences(raw_data)]

tester = ParsePerformanceTester(grammar_string, 
                                sentences)

parser, parser_import = list(NLTK_BEST.items())[0]
nltk_time = 1000*tester.run(parser_import, method="parse")
parser, parser_import = list(MY_PARSER.items())[0]
my_time1 = 1000*tester.run(parser_import, method="parse")
parser, parser_import = list(MY_PARSER.items())[1]
my_time2 = 1000*tester.run(parser_import, method="parse")

print("    sentence length  NLTK    %s ratio    %s ratio2" % (list(MY_PARSER.items())[0][0], list(MY_PARSER.items())[1][0]))
print("    %5.2f            %5.2f    %5.2f    %5.2f    %5.2f    %5.2f" % (tester.average_sentence_length(), nltk_time, my_time1, my_time1/nltk_time, my_time2, my_time2/nltk_time))
