# This Python file uses the following encoding: utf-8
'''
Created on Apr 29, 2011

compute some statistics of my parsers and some nltk parsers
using the toy grammar.

@author: mjacob
'''
from ParsePerformanceTester import ParsePerformanceTester, NLTK_BEST, MY_PARSER
import nltk
from mjacob.algorithms.generate_random import generate_random_sentence
import yaml
import os

grammar = nltk.data.load('grammars/sample_grammars/toy.cfg')
grammar_string = "nltk.data.load('grammars/sample_grammars/toy.cfg')"



for tree_depth in range(4,8):
    sentence_file = 'trivial_sentences_%s.yaml' % (tree_depth,)
    if os.path.exists(sentence_file):
        sentences = yaml.load(open(sentence_file))
    else: 
        sentences = [generate_random_sentence(grammar, tree_depth).split(' ') 
                     for i in range(100)]
        yaml.dump(sentences, open(sentence_file, 'w'))

    tester = ParsePerformanceTester(grammar_string, 
                                    sentences)

    print("%s    %s" % (tree_depth, tester.average_sentence_length()))
    for parser, parser_import in sorted(NLTK_BEST.items()):
        nltk_time = 1000*tester.run(parser_import, method="parse")
        print("        %s    %.2f" % (parser, nltk_time))
    for parser, parser_import in sorted(MY_PARSER.items()):
        my_time = 1000*tester.run(parser_import, method="parse")
        print("        %s    %.2f" % (parser, my_time))
    
    print("   ratio: %s" % (my_time/nltk_time))
