# This Python file uses the following encoding: utf-8
'''
Created on May 17, 2011

@author: mjacob
'''
from timeit import Timer

NLTK_PARSERS = {
               "NLTK's ChartParser": "from nltk import ChartParser",
               "NLTK's LeftCornerChartParser": "from nltk import LeftCornerChartParser",
               "NLTK's BottomUpChartParser":"from nltk import BottomUpChartParser",
               "NLTK's BottomUpLeftCornerChartParser":"from nltk import BottomUpLeftCornerChartParser",
               "NLTK's TopDownChartParser":"from nltk import TopDownChartParser",

               "NLTK's IncrementalLeftCornerChartParser": "from nltk.parse.earleychart import IncrementalLeftCornerChartParser",
               "NLTK's IncrementalChartParser": "from nltk.parse.earleychart import IncrementalChartParser",
               "NLTK's EarleyChartParser": "from nltk.parse.earleychart import EarleyChartParser",
               "NLTK's IncrementalBottomUpChartParser": "from nltk.parse.earleychart import IncrementalBottomUpChartParser",
               "NLTK's IncrementalBottomUpLeftCornerChartParser": "from nltk.parse.earleychart import IncrementalBottomUpLeftCornerChartParser",
               "NLTK's IncrementalTopDownChartParser": "from nltk.parse.earleychart import IncrementalTopDownChartParser",
               }

MY_PARSER = { "TAG ChartParser": "from mjacob.nltk.parse.tag.TagChartParser import TagChartParser",
              "PV TAG ChartParser": "from mjacob.nltk.parse.tag.TagChartParser import PrefixValidTagChartParser",}
NLTK_BEST = { "NLTK's IncrementalLeftCornerChartParser": "from nltk.parse.earleychart import IncrementalLeftCornerChartParser" }

SETUP = """
import nltk
%s as Parser

grammar = %s
sentences = %s
parser = Parser(grammar)
"""

TEST = """
for sentence in sentences:
    parser.%s(sentence)
"""

class ParsePerformanceTester(object):
    """class to use in performance testing parsers"""
    def __init__(self, grammar, sentences, number_of_iterations = 3):
        self.__sentences = sentences
        self.__grammar = grammar
        self.__setup = SETUP
        self.__number_of_iterations = number_of_iterations
        
    def average_sentence_length(self):
        return sum(len(sent) for sent in self.__sentences) / (1.0*len(self.__sentences))
        
    def run(self, parser_import, method="parse"):
        test = TEST % (method)
        timer = Timer(test,
                      self.__setup % (parser_import, 
                                      self.__grammar, 
                                      self.__sentences))
                
        result_time = timer.timeit(number=self.__number_of_iterations)
        return result_time/(self.__number_of_iterations*len(self.__sentences))

