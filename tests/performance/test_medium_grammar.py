# This Python file uses the following encoding: utf-8
'''
Created on Apr 29, 2011

compute some statistics of my parsers and some nltk parsers
using the toy grammar.

@author: mjacob
'''
from ParsePerformanceTester import ParsePerformanceTester, MY_PARSER, NLTK_BEST
import nltk
from mjacob.algorithms.generate_random import generate_random_sentence
import yaml
import os
from mjacob.nltk.grammar.TreeAdjoiningGrammar import TreeAdjoiningGrammar

g = '''S -> NP VP
PP -> P NP
NP -> Det NN
NN -> N | NN PP | AP NN | NN CP
VP -> V1 | V2 NP | VC CP | VP PP | AdvP VP
AP -> A | AP A
AdvP -> Adv | AdvP Adv
CP -> 'that' S | S
Det -> 'a' | 'the' | 'one' | 'no' | 'each'
N -> 'dog' | 'cat' | 'man' | 'hamster' | 'woman' | 'sherif' | 'pug' | 'bird' | 'demon' | 'spider' | 'horse' | 'donkey' | 'farmer' | 'mink' | 'marmot' | 'groundhog' | 'cuy' | 'rainbow'
V1 -> 'sat' | 'slept' | 'yelled' | 'died' | 'wept' | 'sashayed' | 'fell' | 'rose' | 'won' | 'lost'
V2 -> 'chased' | 'hit' | 'saw' | 'heard' | 'shot' | 'beat' | 'fed' | 'lifted' | 'befriended' | 'googled' | 'smelled' | 'sensed'
VC -> 'thought' | 'knew' | 'saw' | 'heard' | 'believed' | 'understood'
P -> 'on' | 'in' | 'under' | 'beside' | 'inside' | 'near' | 'below' | 'above' | 'before' | 'after' | 'through'
A -> 'red' | 'blue' | 'quick' | 'slow' | 'frusty' | 'insane' | 'smarmy' | 'slithy' | 'mimsy' | 'green' | 'yellow' | 'black' | 'white' | 'orange' | 'indigo' | 'violet' | 'purple' | 'colorless' | 'tasty'
Adv -> 'quickly' | 'slowly' | 'completely' | 'entirely' | 'vaguely'
'''

grammar = nltk.parse_cfg(g)
grammar_string = """nltk.parse_cfg('''%s ''')""" % (g)


gr = TreeAdjoiningGrammar(cfg=grammar)
print(gr)
print("nonterminals: %s" % (len(gr.nonterminals())))
print("terminals: %s" % (len(gr.terminals())))

print("    tree depth  sentence length  NLTK    %s ratio    %s ratio2" % (list(MY_PARSER.items())[0][0], list(MY_PARSER.items())[1][0]))
for tree_depth in range(4,11):
    sentence_file = 'medium_sentences_%s.yaml' % (tree_depth,)
    if os.path.exists(sentence_file):
        sentences = yaml.load(open(sentence_file))
    else: 
        sentences = [generate_random_sentence(grammar, tree_depth).split(' ') 
                     for i in range(100)]
        yaml.dump(sentences, open(sentence_file, 'w'))

    tester = ParsePerformanceTester(grammar_string, 
                                    sentences)

    for parser, parser_import in sorted(NLTK_BEST.items()):
        nltk_time = 1000*tester.run(parser_import, method="parse")
    parser, parser_import = list(MY_PARSER.items())[0]
    my_time1 = 1000*tester.run(parser_import, method="parse")
    parser, parser_import = list(MY_PARSER.items())[1]
    my_time2 = 1000*tester.run(parser_import, method="parse")

    print("    %2i         %5.2f            %5.2f    %5.2f    %5.2f    %5.2f    %5.2f" % (tree_depth, tester.average_sentence_length(), nltk_time, my_time1, my_time1/nltk_time, my_time2, my_time2/nltk_time))
